import React, { Component } from 'react';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

import {
  PageHeader, Row, Col, Spin, DatePicker, Form, Button,
  Switch, Table,
} from 'antd';

import * as qs from 'query-string';
import api from '../../apis/blurtopianAPI';

import 'antd/dist/antd.css';
import './Home.css';

am4core.useTheme(am4themes_animated);

const { RangePicker } = DatePicker;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

class AccountsCreatedChart extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isDisplayChart: true,
      items: [],
      loading: false,
    }
  }

  componentWillUnmount() {
    if (this.chart) {
      this.chart.dispose();
    }
  }

  createChart = () => {
    let { items } = this.state;
    let chart = am4core.create("chartdiv", am4charts.XYChart);
    chart.paddingRight = 20;

    let data = [];
    items.forEach(item => {
      let { year, month, day } = item._id;
      data.push({ date: new Date(year, parseInt(month) - 1, day), value: item.count });
    });

    chart.data = data;

    let title = chart.titles.create();
    title.text = "BLURT Accounts Created";
    title.fontSize = 25;
    title.marginBottom = 30;

    let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.title.text = "Date";
    dateAxis.renderer.grid.template.location = 0;

    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.title.text = "Accounts Count";
    valueAxis.tooltip.disabled = true;
    valueAxis.renderer.minWidth = 10;

    let series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.dateX = "date";
    series.dataFields.valueY = "value";

    series.tooltipText = "{valueY.value}";
    chart.cursor = new am4charts.XYCursor();

    this.chart = chart;
  }

  getData = async (query) => {
    let apiUrl = ""
    if (this.state.isDisplayChart) {
      apiUrl = `/api/accounts/created_by_date?${qs.stringify(query)}`;
    } else {
      apiUrl = `/api/accounts/created_by_creator?${qs.stringify(query)}`;
    }
    const response = await api.getUrl(apiUrl);
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  handleFilter = () => {
    const {
      start_date, end_date,
    } = this.state;
    const query = {
      start_date: start_date,
      end_date: end_date,
    };
    this.setState({ loading: true });
    this.getData(query)
      .then(res => {
        this.setState({ items: res.data, loading: false })
        if (this.state.isDisplayChart) {
          this.createChart();
        }
      })
  }

  createTableColumns = async () => {
    this.columns = [
      {
        title: 'Date',
        dataIndex: '_id',
        key: 'date',
        render: _id => <span>{`${_id.year}/${_id.month}/${_id.day}`}</span>,
      },
      {
        title: 'Creator',
        dataIndex: '_id',
        key: 'creator',
        render: _id => <span>{_id.creator}</span>,
      },
      {
        title: 'Accounts Created',
        dataIndex: 'count',
        key: 'count',
        render: count => <span>{count}</span>,
      },
    ];
  }

  render() {
    const { loading, isDisplayChart, items } = this.state
    if (!isDisplayChart) {
      this.createTableColumns();
    }

    let modResult = [];
    let i = 0;
    items.forEach(item => {
      i++;
      modResult.push({ ...item, key: i, rowKey: { _id: item._id, rowNum: i } });
    });

    return (
      <PageHeader>
        <div className="wrap">
          <div className="extraContent">
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12}>
                <Form {...formItemLayout}>
                  <Form.Item label="Range:">
                    <RangePicker
                      format={'YYYY-MM-DD'}
                      onChange={(date, dateString) => this.setState({ start_date: dateString[0], end_date: dateString[1]}) }
                    />
                  </Form.Item>
                  <Form.Item label="Chart/Table">
                    <Switch
                      style={{ marginLeft: 5 }}
                      checkedChildren="Chart"
                      unCheckedChildren="Table"
                      defaultChecked={isDisplayChart}
                      onChange={(checked) => {
                        this.setState({ isDisplayChart: checked, isSwitch: true });
                        this.setState({ start_date: null, end_date: null})
                      }}
                    />
                  </Form.Item>
                  <Form.Item {...tailLayout}>
                    <Button
                      onClick={this.handleFilter}
                    > Filter</Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
            {loading &&
              <Row type="flex" justify="center">
                <Col xs={24} sm={24} md={24} lg={12} style={{ textAlign: "center" }}>
                  <Spin size="large" />
                </Col>
              </Row>
            }
            {!loading && isDisplayChart &&
              <Row gutter={16} style={{margin: "10px"}}>
                <Col xs={24} sm={24} md={24} lg={24}>
                  <div id="chartdiv" style={{ width: "100%", height: "500px" }}></div>
                </Col>
              </Row>
            }
            {!loading && !isDisplayChart &&
              <Row gutter={16} style={{margin: "10px"}}>
                <Col xs={24} sm={24} md={24} lg={24}>
                  <Table
                    columns={this.columns}
                    dataSource={modResult}
                    pagination={false}
                  />
                </Col>
              </Row>
            }
          </div>
        </div>
      </PageHeader>

    );
  }
}

export default AccountsCreatedChart;