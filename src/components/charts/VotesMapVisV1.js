import React, { Component } from 'react';
import { Network } from 'vis-network';
import { DataSet } from 'vis-data';

import {
  PageHeader, Row, Col, Spin, Form, Button, Input,
  DatePicker,
} from 'antd';

import * as qs from 'query-string';
import api from '../../apis/blurtopianAPI';
import moment from 'moment';
import _ from 'lodash';

import 'antd/dist/antd.css';
import './Home.css';

const { RangePicker } = DatePicker;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

class VotesMapVisV1 extends Component {

  constructor(props) {
    super(props)
    this.state = {
      account: '',
      start_date: '',
      end_date: '',
      l0_account_votes: [],
      l1_account_votes: [],
      l2_account_votes: [],
      l3_account_votes: [],
      loading: false,
    }
  }

  componentWillUnmount() {
    if (this.chart) {
      this.chart.dispose();
    }
  }

  createChart = async () => {
    let {
      account,
      l0_account_votes,
      l1_account_votes,
    } = this.state;

    let node_data = [];
    let has_self_vote = false;
    l0_account_votes.forEach(item => {
      let { author } = item._id;
      if (account === author) has_self_vote = true;
      node_data.push({ id: author, label: author, value: item.count });
    });
    if (!has_self_vote) node_data.push({ id: account, label: account, value: 1 });
    var nodes = new DataSet(node_data);

    // create an array with edges
    let edge_data = [];
    l0_account_votes.forEach(item => {
      let { voter, author } = item._id;
      edge_data.push({
        from: voter,
        to: author,
        arrows: { to: true },
        smooth: { enabled: true, type: 'curvedCW' },
        color: 'blue',
      });
    });
    l1_account_votes.forEach(item => {
      let { voter, author } = item._id;
      edge_data.push({
        from: voter,
        to: author,
        arrows: { to: true },
        color: 'red',
      });
    });
    var edges = new DataSet(edge_data);

    // create a network
    var container = document.getElementById("chartdiv");
    var data = {
      nodes: nodes,
      edges: edges,
    };
    var options = {

      "edges": {
        "smooth": false
      },

      "physics": {
        "hierarchicalRepulsion": {
          "centralGravity": 0,
          "springConstant": 0
        },
        "minVelocity": 0.1,
        "maxVelocity": 1.5,
        "solver": "hierarchicalRepulsion"
      },

      interaction: {
        hover: true
      },

      nodes: {
        shape: 'dot',
        scaling: {
          customScalingFunction: function (min, max, total, value) {
            return value / total;
          },
          min: 10,
          max: 100
        }
      }
    };
    new Network(container, data, options);

  }

  getData = async (query) => {
    const response = await api.getUrl(`/api/votes/outvotes?${qs.stringify(query)}`);
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  handleFilter = () => {
    let {
      account, start_date, end_date,
    } = this.state;
    start_date = start_date ? start_date : moment().subtract(7,'d').format("YYYY-MM-DD");
    end_date = end_date ? end_date : moment().format("YYYY-MM-DD");
    this.setState({ loading: true })
    this.getData({
      voters: account,
      start_date,
      end_date,
    })
      .then(res => {
        this.setState({ l0_account_votes: res.data })
        let l1_accounts = [];
        res.data.forEach(item => {
          l1_accounts.push(item._id.author)
        });
        this.getData({
          voters: l1_accounts,
          author: account,
          start_date: start_date,
          end_date: end_date,
        }).then(res => {
          this.setState({ l1_account_votes: res.data, loading: false })
          this.createChart();
        })
      })
  }

  render() {
    const { loading, account } = this.state
    const disable_filter = _.isEmpty(account);

    return (
      <PageHeader>
        <div className="wrap">
          <div className="extraContent">
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12}>
                <Form {...formItemLayout}>
                  <Form.Item label="Range">
                    <RangePicker
                      format={'YYYY-MM-DD'}
                      defaultValue={[moment().subtract(7,'d'), moment()]}
                      onChange={(date, dateString) => this.setState({ start_date: date[0], end_date: date[1]}) }
                    />
                  </Form.Item>
                  <Form.Item label="Account">
                    <Input
                      onChange={e => this.setState({ account: e.target.value })}
                    />
                  </Form.Item>
                  <Form.Item {...tailLayout}>
                    <Button
                      onClick={this.handleFilter}
                      disabled={disable_filter}
                    > Filter</Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
            {loading &&
              <Row type="flex" justify="center">
                <Col xs={24} sm={24} md={24} lg={12} style={{ textAlign: "center" }}>
                  <Spin size="large" />
                </Col>
              </Row>
            }
            {!loading &&
              <Row>
                <Col xs={24} sm={24} md={24} lg={24}>
                  <div id="chartdiv" style={{ width: "100%", height: "500px" }}></div>
                </Col>
              </Row>
            }
          </div>
        </div>
      </PageHeader>

    );
  }
}

export default VotesMapVisV1;