import React, { Component } from 'react';
import {
  PageHeader, Row, Col, Spin, Table, Tag,
} from 'antd';

import moment from 'moment';
import * as qs from 'query-string';
import api from '../../apis/blurtopianAPI';

import 'antd/dist/antd.css';
import './Home.css';

class Richlist extends Component {

  constructor(props) {
    super(props)
    this.state = {
      voter: '',
      items: [],
      loading: false,
    }
  }

  componentWillMount() {
    let params = qs.parse(this.props.location.search)
    this.setState({ loading: true })
    this.getData(params)
      .then(res => {
        this.setState({ items: res.data, loading: false })
        //this.createData(res.data);
      })
  }

  getData = async (query) => {
    console.log('query', query)
    const response = await api.getUrl(`/api/richlist?${qs.stringify(query)}`);
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  createTableColumns = async () => {
    this.columns = [
      {
        title: 'No',
        dataIndex: 'key',
        key: 'key',
        render: key => <span>{key}</span>,
      },
      {
        title: 'Author',
        dataIndex: 'name',
        key: 'name',
        render: name => <span>{name}</span>,
      },
      {
        title: 'Balance',
        dataIndex: 'balance',
        key: 'balance',
        sorter: (a, b) => a.balance - b.balance,
        render: balance => <span>{balance}</span>,
      },
      {
        title: 'BP',
        dataIndex: 'vesting_shares',
        key: 'vesting_shares',
        defaultSortOrder: 'descend',
        sorter: (a, b) => a.vesting_shares - b.vesting_shares,
        render: vesting_shares => <span>{vesting_shares}</span>,
      },
      {
        title: 'Delegated',
        dataIndex: 'delegated_vesting_shares',
        key: 'delegated_vesting_shares',
        sorter: (a, b) => a.delegated_vesting_shares - b.delegated_vesting_shares,
        render: delegated_vesting_shares => <span>{delegated_vesting_shares}</span>,
      },
      {
        title: 'Received Delegation',
        dataIndex: 'received_vesting_shares',
        key: 'received_vesting_shares',
        sorter: (a, b) => a.received_vesting_shares - b.received_vesting_shares,
        render: received_vesting_shares => <span>{received_vesting_shares}</span>,
      },
      {
        title: 'Powering Down',
        key: 'power_down',
        sorter: (a, b) => a.vesting_withdraw_rate - b.vesting_withdraw_rate,
        render: (text, record) => (
          <span>
            {
              moment().diff(moment.utc(record.next_vesting_withdrawal)) < 0 ?
              <Tag color="blue">{record.vesting_withdraw_rate} {moment.utc(record.next_vesting_withdrawal).fromNow()}</Tag>
              :
              null
            }
          </span>
        ),
      },
    ];
  }

  render() {
    this.createTableColumns();
    const { loading, items } = this.state

    let modResult = [];
    let i = 0;
    items.forEach(item => {
      i++;
      modResult.push({ ...item, key: i, rowKey: { _id: item._id, rowNum: i } });
    });

    return (
      <PageHeader>
        <div className="wrap">
          <div className="extraContent">
            {loading &&
              <Row type="flex" justify="center">
                <Col xs={24} sm={24} md={24} lg={16} style={{ textAlign: "center" }}>
                  <Spin size="large" />
                </Col>
              </Row>
            }
            {!loading &&
              <Row>
                <Col xs={24} sm={24} md={24} lg={24}>
                  <Table
                    columns={this.columns}
                    dataSource={modResult}
                    pagination={{
                      hideOnSinglePage: true,
                      pageSize: 1000,
                    }}
                  />
                </Col>
              </Row>
            }
          </div>
        </div>
      </PageHeader>

    );
  }
}

export default Richlist;