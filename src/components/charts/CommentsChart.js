import React, { Component } from 'react';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";

import { PageHeader, Row, Col, Spin, } from 'antd';

import moment from 'moment';
import api from '../../apis/blurtopianAPI';

import 'antd/dist/antd.css';
import './Home.css';

class CommentsChart extends Component {

  constructor(props) {
    super(props)
    this.state = {
      record: {},
      loadingRecord: true,
    }
  }

  componentDidMount() {
    this.getRequiredInfoFromAPI();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.location !== this.props.location) {
      this.getRequiredInfoFromAPI();
    }
  }

  componentWillUnmount() {
    if (this.chart) {
      this.chart.dispose();
    }
  }

  getRequiredInfoFromAPI = async () => {
    this.setState({ loadingRecord: true });
    this.getRecord(true)
      .then(res => {
        this.setState({ record: res.data, loadingRecord: false });

        let chart = am4core.create("chartdiv", am4charts.XYChart);
        chart.paddingRight = 20;

        let data = [];
        res.data.forEach(item => {
          let { _id, root_post_count, comment_count } = item
          let {
            year, month, day,
          } = _id;
          data.push({
            date: new Date(year, parseInt(month) - 1, day),
            root_post_count: root_post_count,
            comment_count: comment_count,
          });
        });
        chart.data = data;

        let title = chart.titles.create();
        title.text = "BLURT Posts and Comments Count";
        title.fontSize = 25;
        title.marginBottom = 30;

        let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
        dateAxis.title.text = "Date";
        dateAxis.renderer.grid.template.location = 0;

        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = "Comment Count";
        valueAxis.tooltip.disabled = true;
        valueAxis.renderer.minWidth = 10;

        function createSeries(field, name) {
          // Set up series
          var series = chart.series.push(new am4charts.ColumnSeries());
          series.name = name;
          series.dataFields.valueY = field;
          series.dataFields.dateX = "date";
          series.sequencedInterpolation = true;

          // Make it stacked
          series.stacked = true;

          // Configure columns
          series.columns.template.width = am4core.percent(60);
          series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";

          // Add label
          var labelBullet = series.bullets.push(new am4charts.LabelBullet());
          labelBullet.label.text = "{valueY}";
          labelBullet.locationY = 0.5;
          labelBullet.label.hideOversized = true;

          return series;
        }
        createSeries("root_post_count", "Posts");
        createSeries("comment_count", "Comments");

        /*
        let series = chart.series.push(new am4charts.ColumnSeries());
        series.name = "Root Posts";
        series.dataFields.dateX = "date";
        series.dataFields.valueY = "root_post_count";
        series.stacked = true;
        series.tooltipText = "{valueY.value}";

        let series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.name = "Comments";
        series2.dataFields.dateX = "date";
        series2.dataFields.valueY = "comment_count";
        series2.stacked = true;
        series2.tooltipText = "{valueY.value}";
        */

        chart.cursor = new am4charts.XYCursor();

        this.chart = chart;
      })
  }

  getRecord = async (is_root_post) => {
    const response = await api.getUrl(`/api/comments/by_date?start_date=2020-07-10&end_date=${moment().format("YYYY-MM-DD")}&posts_only=${is_root_post}`);
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  render() {
    const { loadingRecord } = this.state
    if (loadingRecord) {
      return (
        <div className="wrap">
          <div className="extraContent">
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12} style={{ textAlign: "center" }}>
                <Spin size="large" />
              </Col>
            </Row>
          </div>
        </div>
      )
    }

    return (
      <PageHeader>
        <div className="wrap">
          <div className="extraContent">
            <Row gutter={16} style={{margin: "10px"}}>
              <Col xs={24} sm={24} md={24} lg={24}>
                <div id="chartdiv" style={{ width: "100%", height: "500px" }}></div>
              </Col>
            </Row>
          </div>
        </div>
      </PageHeader>

    );
  }
}

export default CommentsChart;