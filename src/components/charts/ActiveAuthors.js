import React, { Component } from 'react';

import * as am4core from "@amcharts/amcharts4/core";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

import {
  PageHeader, Row, Col, Spin, DatePicker, Form, Button,
  Table,
} from 'antd';

import * as qs from 'query-string';
import moment from 'moment';
import api from '../../apis/blurtopianAPI';
import blurt from'@blurtfoundation/blurtjs';

import 'antd/dist/antd.css';
import './Home.css';

blurt.api.setOptions({ url: 'https://rpc.blurt.world', useAppbaseApi: true });
am4core.useTheme(am4themes_animated);

const { RangePicker } = DatePicker;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

class ActiveAuthors extends Component {

  constructor(props) {
    super(props)
    this.state = {
      start_date: moment().subtract(180,'d'),
      end_date: moment(),
      items: [],
      loading: true,
    }
  }

  componentDidMount() {
    this.getRequiredInfoFromAPI();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.location !== this.props.location) {
      this.getRequiredInfoFromAPI();
    }
  }

  getRequiredInfoFromAPI = async () => {
    this.setState({ loading: true });
    this.getItems({
        start_date: this.state.start_date.format("YYYY-MM-DD"),
        end_date: this.state.end_date.format("YYYY-MM-DD"),
      })
      .then(res => {
        this.setState({ items: res.data, loading: false });
      })
  }

  getItems = async (query) => {
    const response = await api.getUrl(`/api/accounts/active_authors_with_bp_comment?${qs.stringify(query)}`);
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  handleFilter = () => {
    const query = {
      start_date: this.state.start_date.format("YYYY-MM-DD"),
      end_date: this.state.end_date.format("YYYY-MM-DD"),
    };
    this.setState({ loading: true });
    this.getItems(query)
      .then(res => {
        this.setState({ items: res.data, loading: false });
      })
  }

  createTableColumns = async () => {
    this.columns = [
      {
        title: 'Author',
        dataIndex: 'name',
        key: 'name',
        render: name => <span>{name}</span>,
      },
      {
        title: 'BP',
        dataIndex: 'bp',
        key: 'bp',
        defaultSortOrder: 'descend',
        sorter: (a, b) => a.bp - b.bp,
        render: bp => <span>{bp}</span>,
      },

      {
        title: 'Latest Post/Comment',
        dataIndex: 'latest_post',
        key: 'latest_post',
        render: (text, record) => (
          <a
            href={`https://blurt.blog/@${record.name}/${record.latest_post.permlink}`}
            target="_blank"
            rel="noopener noreferrer"
          >Link</a>
        ),
      },
    ];
  }

  render() {
    this.createTableColumns();
    const { loading, items } = this.state;

    return (
      <PageHeader>
        <div className="wrap">
          <div className="extraContent">
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={18}
                style={{ display: "flex", justifyContent: "center" }}
              >
                <h2>Active Authors</h2>
              </Col>
            </Row>
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12}>
                <Form {...formItemLayout}>
                  <Form.Item label="Range:">
                    <RangePicker
                      format={'YYYY-MM-DD'}
                      defaultValue={[this.state.start_date, this.state.end_date]}
                      onChange={(date, dateString) => this.setState({ start_date: date[0], end_date: date[1]}) }
                    />
                  </Form.Item>
                  <Form.Item {...tailLayout}>
                    <Button
                      onClick={this.handleFilter}
                    > Filter</Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
            {loading &&
              <Row type="flex" justify="center">
                <Col xs={24} sm={24} md={24} lg={12} style={{ textAlign: "center" }}>
                  <Spin size="large" />
                </Col>
              </Row>
            }
            {!loading &&
              <Row gutter={16} style={{margin: "10px"}}>
                <Col xs={24} sm={24} md={24} lg={24}>
                  <Table
                    columns={this.columns}
                    dataSource={items}
                    pagination={false}
                  />
                </Col>
              </Row>
            }
          </div>
        </div>
      </PageHeader>

    );
  }
}

export default ActiveAuthors;