import React, { Component } from 'react';
import {
  PageHeader, Row, Col, Spin, Form, Button, Input,
  DatePicker, Table, Tag,
} from 'antd';

import * as qs from 'query-string';
import api from '../../apis/blurtopianAPI';
import moment from 'moment';

import 'antd/dist/antd.css';
import './Home.css';

const { RangePicker } = DatePicker;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

class VotesTable extends Component {

  constructor(props) {
    super(props)
    this.state = {
      voter: '',
      start_date: '',
      end_date: '',
      items: [],
      loading: false,
    }
  }

  componentWillUnmount() {
    if (this.chart) {
      this.chart.dispose();
    }
  }

  getData = async (query) => {
    const response = await api.getUrl(`/api/votes/vote_map?${qs.stringify(query)}`);
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  handleFilter = () => {
    const {
      voter, author, start_date, end_date,
    } = this.state;
    const query = {
      voter: voter,
      author: author,
      start_date: start_date ? start_date : moment().subtract(7,'d').format("YYYY-MM-DD"),
      end_date: end_date ? end_date : moment().format("YYYY-MM-DD"),
    };
    this.setState({ loading: true })
    this.getData(query)
      .then(res => {
        this.setState({ loading: false })
        this.createData(res.data);
      })
  }

  createData = (items) => {
    let voter_author_map = {};
    let lvl1_data = [];
    // convert to array; extract voter-author map
    items.forEach(item => {
      let { author, voter } = item._id;
      lvl1_data.push({ author: author, voter: voter, count: item.count });

      if (!voter_author_map[voter]) {
        voter_author_map[voter] = { };
        voter_author_map[voter][author] = "";
      } else {
        voter_author_map[voter][author] = "";
      }
    });

    let lvl2_data = {};
    // convert to map; reduce vote counts to total;
    lvl1_data.forEach(item => {
      let { author, voter, count } = item;
      if (!lvl2_data[author]) {
        lvl2_data[author] = { votes: count, voters: {} }
        lvl2_data[author].voters[voter]= { votes: count };

      } else {
        lvl2_data[author].votes = lvl2_data[author].votes + count

        if (!lvl2_data[author].voters[voter]) {
          lvl2_data[author].voters[voter] = { votes: count }
        } else {
          lvl2_data[author].voters[voter].votes = lvl2_data[author].voters[voter].votes + count;
        }
      }
    });

    let lvl3_data = [];
    let authors = Object.keys(lvl2_data)
    authors.forEach(author => {
      let info = lvl2_data[author];

      let voter_accounts = Object.keys(info.voters);
      let voters = [];
      voter_accounts.forEach(voter => {
        voters.push({
          name: voter,
          votes: info.voters[voter].votes,
        })
      })
      lvl3_data.push({
        name: author,
        votes: info.votes,
        voters: voters
      });
    })

    this.setState({ items: lvl3_data, loading: false })
  }

  createTableColumns = async () => {
    this.columns = [
      {
        title: 'Author',
        dataIndex: 'name',
        key: 'name',
        render: name => <span>{name}</span>,
      },
      {
        title: 'Received Votes',
        dataIndex: 'votes',
        key: 'votes',
        defaultSortOrder: 'descend',
        sorter: (a, b) => a.votes - b.votes,
        render: votes => <span>{votes}</span>,
      },
      {
        title: 'Voters',
        key: 'voters',
        render: (text, record) => (
          <span>
            {
              record.voters.map(voter => {
                return <Tag color="blue">{`${voter.name}: ${voter.votes}`}</Tag>
              })
            }
          </span>
        ),
      },
    ];
  }

  render() {
    this.createTableColumns();
    const { loading, voter, author, items } = this.state

    let modResult = [];
    let i = 0;
    items.forEach(item => {
      i++;
      modResult.push({ ...item, key: i, rowKey: { _id: item._id, rowNum: i } });
    });

    return (
      <PageHeader>
        <div className="wrap">
          <div className="extraContent">
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12}>
                <Form {...formItemLayout}>
                  <Form.Item label="Range">
                    <RangePicker
                      format={'YYYY-MM-DD'}
                      defaultValue={[moment().subtract(7,'d'), moment()]}
                      onChange={(date, dateString) => this.setState({ start_date: date[0], end_date: date[1]}) }
                    />
                  </Form.Item>
                  <Form.Item label="Voter">
                    <Input
                      defaultValue={voter}
                      onChange={e => this.setState({ voter: e.target.value })}
                    />
                  </Form.Item>
                  <Form.Item label="Author">
                    <Input
                      defaultValue={author}
                      onChange={e => this.setState({ author: e.target.value })}
                    />
                  </Form.Item>
                  <Form.Item {...tailLayout}>
                    <Button
                      onClick={this.handleFilter}
                    > Filter</Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
            {loading &&
              <Row type="flex" justify="center">
                <Col xs={24} sm={24} md={24} lg={12} style={{ textAlign: "center" }}>
                  <Spin size="large" />
                </Col>
              </Row>
            }
            {!loading &&
              <Row>
                <Col xs={24} sm={24} md={24} lg={24}>
                  <Table
                    columns={this.columns}
                    dataSource={modResult}
                    pagination={{hideOnSinglePage: true}}
                  />
                </Col>
              </Row>
            }
          </div>
        </div>
      </PageHeader>

    );
  }
}

export default VotesTable;