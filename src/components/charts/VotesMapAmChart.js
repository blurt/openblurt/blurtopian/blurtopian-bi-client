import React, { Component } from 'react';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4plugins_forceDirected from "@amcharts/amcharts4/plugins/forceDirected";

import {
  PageHeader, Row, Col, Spin, Form, Button, Input,
  DatePicker,
} from 'antd';

import * as qs from 'query-string';
import api from '../../apis/blurtopianAPI';
import moment from 'moment';

import 'antd/dist/antd.css';
import './Home.css';

const { RangePicker } = DatePicker;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

class VotesMapAmChart extends Component {

  constructor(props) {
    super(props)
    this.state = {
      voter: '',
      start_date: '',
      end_date: '',
      data: [],
      loading: false,
    }
  }

  componentWillUnmount() {
    if (this.chart) {
      this.chart.dispose();
    }
  }

  createChart = () => {
    let { items } = this.state;
    let chart = am4core.create("chartdiv", am4plugins_forceDirected.ForceDirectedTree);
    chart.paddingRight = 20;

    let voter_author_map = {};
    let lvl1_data = [];
    // convert to array; extract voter-author map
    items.forEach(item => {
      let { author, voter } = item._id;
      lvl1_data.push({ author: author, voter: voter, count: item.count });

      if (!voter_author_map[voter]) {
        voter_author_map[voter] = { };
        voter_author_map[voter][author] = "";
      } else {
        voter_author_map[voter][author] = "";
      }
    });

    let lvl2_data = {};
    // convert to map; reduce vote counts to total;
    lvl1_data.forEach(item => {
      let { author, voter, count } = item;
      if (!lvl2_data[author]) {
        lvl2_data[author] = { votes: count, voters: {} }
        lvl2_data[author].voters[voter]= { votes: count };

      } else {
        lvl2_data[author].votes = lvl2_data[author].votes + count

        if (!lvl2_data[author].voters[voter]) {
          lvl2_data[author].voters[voter] = { votes: count }
        } else {
          lvl2_data[author].voters[voter].votes = lvl2_data[author].voters[voter].votes + count;
        }
      }
    });

    let lvl3_data = [];
    let authors = Object.keys(lvl2_data)
    authors.forEach(author => {
      let info = lvl2_data[author];

      let voter_accounts = Object.keys(info.voters);
      let voters = [];
      voter_accounts.forEach(voter => {
        voters.push({
          name: voter,
          votes: info.voters[voter].votes,
          link: Object.keys(voter_author_map[voter])
        })
      })
      lvl3_data.push({
        name: author,
        votes: info.votes,
        voters: voters
      });
    })
    console.log('lvl3_data', lvl3_data)

    let title = chart.titles.create();
    title.text = "BLURT Vote Map";
    title.fontSize = 25;
    title.marginBottom = 30;

    let series = chart.series.push(new am4plugins_forceDirected.ForceDirectedSeries());
    series.data = lvl3_data.slice(0, 50);

    // Set up data fields
    series.dataFields.value = "votes";
    series.dataFields.name = "name";
    series.dataFields.children = "voters";
    series.dataFields.id = "name";
    series.dataFields.linkWith = "voters";

    // Add labels
    series.nodes.template.label.text = "{name}";
    series.nodes.template.tooltipText = "{name}: [bold]{votes}[/]";
    series.fontSize = 10;
    series.minRadius = 15;
    series.maxRadius = 40;
    series.centerStrength = 0.5;

    chart.zoomable = true;
    this.chart = chart;

  }

  getData = async (query) => {
    const response = await api.getUrl(`/api/votes/vote_map?${qs.stringify(query)}`);
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  handleFilter = () => {
    const {
      voter, author, start_date, end_date,
    } = this.state;
    const query = {
      voter: voter,
      author: author,
      start_date: start_date ? start_date : moment().subtract(7,'d').format("YYYY-MM-DD"),
      end_date: end_date ? end_date : moment().format("YYYY-MM-DD"),
    };
    this.getData(query)
      .then(res => {
        this.setState({ items: res.data, loading: false })
        this.createChart();
      })
  }

  render() {
    const { loading, voter, author } = this.state
    if (loading) {
      return (
        <div className="wrap">
          <div className="extraContent">
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12} style={{ textAlign: "center" }}>
                <Spin size="large" />
              </Col>
            </Row>
          </div>
        </div>
      )
    }

    return (
      <PageHeader>
        <div className="wrap">
          <div className="extraContent">
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12}>
                <Form {...formItemLayout}>
                  <Form.Item label="Range">
                    <RangePicker
                      format={'YYYY-MM-DD'}
                      defaultValue={[moment().subtract(7,'d'), moment()]}
                      onChange={(date, dateString) => this.setState({ start_date: date[0], end_date: date[1]}) }
                    />
                  </Form.Item>
                  <Form.Item label="Voter">
                    <Input
                      defaultValue={voter}
                      onChange={e => this.setState({ voter: e.target.value })}
                    />
                  </Form.Item>
                  <Form.Item label="Author">
                    <Input
                      defaultValue={author}
                      onChange={e => this.setState({ author: e.target.value })}
                    />
                  </Form.Item>
                  <Form.Item {...tailLayout}>
                    <Button
                      onClick={this.handleFilter}
                    > Filter</Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
            <Row>
              <Col xs={24} sm={24} md={24} lg={24}>
                <div id="chartdiv" style={{ width: "100%", height: "500px" }}></div>
              </Col>
            </Row>
          </div>
        </div>
      </PageHeader>

    );
  }
}

export default VotesMapAmChart;