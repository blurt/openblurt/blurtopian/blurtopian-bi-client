import React, { Component } from 'react';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

import {
  PageHeader, Row, Col, Spin, DatePicker, Form, Button, InputNumber,
  Switch, Table,
} from 'antd';

import * as qs from 'query-string';
import moment from 'moment';
import api from '../../apis/blurtopianAPI';

import 'antd/dist/antd.css';
import './Home.css';

am4core.useTheme(am4themes_animated);

const { RangePicker } = DatePicker;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

class VotesByCount extends Component {

  constructor(props) {
    super(props)
    this.state = {
      start_date: '',
      end_date: '',
      isDisplayChart: true,
      items: [],
      account_count: 100,
      loading: true,
    }
  }

  componentDidMount() {
    this.getRequiredInfoFromAPI();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.location !== this.props.location) {
      this.getRequiredInfoFromAPI();
    }
  }

  componentWillUnmount() {
    console.log('this.state.isSwitch', this.state.isSwitch)
    if(this.state.isSwitch) {
    } else {
      if (this.chart) {
        this.chart.dispose();
      }
    }
  }

  getRequiredInfoFromAPI = async () => {
    this.setState({ loading: true });
    this.getData({
        start_date: moment().subtract(7,'d').format("YYYY-MM-DD"),
        end_date: moment().format("YYYY-MM-DD"),
        include_author: false,
      })
      .then(res => {
        this.setState({ items: res.data.splice(0, this.state.account_count), loading: false });
        this.createChart();
      })
  }

  createChart = () => {
    let { items, account_count } = this.state
    let res_data = items.splice(0, account_count);
    let chart = am4core.create("chartdiv", am4charts.XYChart);

    chart.paddingRight = 20;

    let data = [];
    res_data.forEach(item => {
      let { voter } = item._id;
      data.push({ voter: voter, value: item.count });
    });

    chart.data = data;

    let title = chart.titles.create();
    title.text = "BLURT Vote Count";
    title.fontSize = 25;
    title.marginBottom = 30;

    // Create axes
    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "voter";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.labels.template.rotation = -90;
    categoryAxis.renderer.labels.template.horizontalCenter = "right";
    categoryAxis.renderer.labels.template.verticalCenter = "middle";
    categoryAxis.renderer.minGridDistance = 30;

    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.title.text = "Vote Count";
    valueAxis.tooltip.disabled = true;
    valueAxis.renderer.minWidth = 10;

    // Create series
    var series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = "value";
    series.dataFields.categoryX = "voter";
    series.name = "Votes";
    series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
    series.columns.template.fillOpacity = .8;

    var columnTemplate = series.columns.template;
    columnTemplate.strokeWidth = 2;
    columnTemplate.strokeOpacity = 1;

    chart.cursor = new am4charts.XYCursor();

    this.chart = chart;
  }

  getData = async (query) => {
    const response = await api.getUrl(`/api/votes/by_count?${qs.stringify(query)}`);
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  handleFilter = () => {
    const {
      start_date, end_date, account_count,
    } = this.state;
    const query = {
      start_date: start_date,
      end_date: end_date,
      include_author: false,
    };
    this.setState({ loading: true });
    this.getData(query)
      .then(res => {
        this.setState({ items: res.data.slice(0, account_count), loading: false })
        this.createChart();
      })
  }

  createTableColumns = async () => {
    this.columns = [
      {
        title: 'No',
        dataIndex: 'rowKey',
        key: 'roKey._id',
        render: rowKey => <span>{rowKey.rowNum}</span>,
      },
      {
        title: 'Voter',
        dataIndex: '_id',
        key: 'voter',
        render: _id => <span>{_id.voter}</span>,
      },
      {
        title: 'Vote Count',
        dataIndex: 'count',
        key: 'count',
        render: count => <span>{count}</span>,
      },
    ];
  }

  render() {
    this.createTableColumns();
    const { loading, isDisplayChart, items, account_count } = this.state
    let modResult = [];
    let i = 0;
    items.forEach(item => {
      i++;
      modResult.push({ ...item, key: i, rowKey: { _id: item._id, rowNum: i } });
    });

    return (
      <PageHeader>
        <div className="wrap">
          <div className="extraContent">
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12}>
                <Form {...formItemLayout}>
                  <Form.Item label="Range:">
                    <RangePicker
                      format={'YYYY-MM-DD'}
                      defaultValue={[moment().subtract(7,'d'), moment()]}
                      onChange={(date, dateString) => this.setState({ start_date: dateString[0], end_date: dateString[1]}) }
                    />
                  </Form.Item>
                  <Form.Item label="No. of Accounts">
                    <InputNumber
                      defaultValue={account_count}
                      onChange={value => this.setState({ account_count: value })}
                    />
                  </Form.Item>
                  <Form.Item label="Chart/Table">
                    <Switch
                      style={{ marginLeft: 5 }}
                      checkedChildren="Chart"
                      unCheckedChildren="Table"
                      defaultChecked={isDisplayChart}
                      onChange={(checked) => {
                        this.setState({ isDisplayChart: checked, isSwitch: true });
                        this.handleFilter();
                      }}
                    />
                  </Form.Item>
                  <Form.Item {...tailLayout}>
                    <Button
                      onClick={this.handleFilter}
                    > Filter</Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
            {loading &&
              <Row type="flex" justify="center">
                <Col xs={24} sm={24} md={24} lg={12} style={{ textAlign: "center" }}>
                  <Spin size="large" />
                </Col>
              </Row>
            }
            {!loading && isDisplayChart &&
              <Row gutter={16} style={{margin: "10px"}}>
                <Col xs={24} sm={24} md={24} lg={24}>
                  <div id="chartdiv" style={{ width: "100%", height: "500px" }}></div>
                </Col>
              </Row>
            }
            {!loading && !isDisplayChart &&
              <Row gutter={16} style={{margin: "10px"}}>
                <Col xs={24} sm={24} md={24} lg={24}>
                  <Table
                    columns={this.columns}
                    dataSource={modResult}
                    pagination={false}
                  />
                </Col>
              </Row>
            }
          </div>
        </div>
      </PageHeader>

    );
  }
}

export default VotesByCount;