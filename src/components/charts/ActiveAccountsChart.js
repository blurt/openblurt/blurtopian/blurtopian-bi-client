import React, { Component } from 'react';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import amcharts from "@amcharts/amcharts4/themes/amcharts";

import {
  PageHeader, Row, Col, Spin, Form, Button, Input,
  DatePicker,
} from 'antd';

import * as qs from 'query-string';
import moment from 'moment';
import api from '../../apis/blurtopianAPI';

import 'antd/dist/antd.css';
import './Home.css';

am4core.useTheme(amcharts);

const { RangePicker } = DatePicker;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

class ActiveAccountsChart extends Component {

  constructor(props) {
    super(props)
    this.state = {
      voter: '',
      start_date: '',
      end_date: '',
      data: [],
      loading: true,
    }
  }

  componentDidMount() {
    this.getRequiredInfoFromAPI();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.location !== this.props.location) {
      this.getRequiredInfoFromAPI();
    }
  }

  componentWillUnmount() {
    if (this.chart) {
      this.chart.dispose();
    }
  }

  getRequiredInfoFromAPI = async () => {
    this.setState({ loading: true });
    this.getData({
        start_date: moment('2020-01-01').format("YYYY-MM-DD"),
        end_date: moment().format("YYYY-MM-DD"),
      })
      .then(res => {
        this.setState({ items: res.data, loading: false });
        this.createChart();
      })
  }

  createChart = () => {
    let { items } = this.state;
    let chart = am4core.create("chartdiv", am4charts.XYChart);
    chart.paddingRight = 20;

    let data = [];
    items.forEach(item => {
      let { year, month, day } = item._id;
      data.push({ date: new Date(year, parseInt(month) - 1, day), value: item.count });
    });

    chart.data = data;

    let title = chart.titles.create();
    title.text = "BLURT Vote Count";
    title.fontSize = 25;
    title.marginBottom = 30;

    let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.title.text = "Date";
    dateAxis.renderer.grid.template.location = 0;

    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.title.text = "Vote Count";
    valueAxis.tooltip.disabled = true;
    valueAxis.renderer.minWidth = 10;

    let series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.dateX = "date";
    series.dataFields.valueY = "value";

    series.tooltipText = "{valueY.value}";
    chart.cursor = new am4charts.XYCursor();

    this.chart = chart;
  }

  getData = async (query) => {
    const response = await api.getUrl(`/api/votes/by_date?${qs.stringify(query)}`);
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  handleFilter = () => {
    const {
      voter, start_date, end_date,
    } = this.state;
    const query = {
      voter: voter,
      start_date: start_date,
      end_date: end_date,
    };
    this.getData(query)
      .then(res => {
        this.setState({ items: res.data, loading: false })
        this.createChart();
      })
  }

  render() {
    const { loading, voter } = this.state
    if (loading) {
      return (
        <div className="wrap">
          <div className="extraContent">
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12} style={{ textAlign: "center" }}>
                <Spin size="large" />
              </Col>
            </Row>
          </div>
        </div>
      )
    }

    return (
      <PageHeader>
        <div className="wrap">
          <div className="extraContent">
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12}>
                <Form {...formItemLayout}>
                  <Form.Item label="Range">
                    <RangePicker
                      format={'YYYY-MM-DD'}
                      defaultValue={[moment('2020-07-04'), moment()]}
                      onChange={(date, dateString) => this.setState({ start_date: dateString[0], end_date: dateString[1]}) }
                    />
                  </Form.Item>
                  <Form.Item label="Account">
                    <Input
                      defaultValue={voter}
                      onChange={e => this.setState({ voter: e.target.value })}
                    />
                  </Form.Item>
                  <Form.Item {...tailLayout}>
                    <Button
                      onClick={this.handleFilter}
                    > Filter</Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
            <Row gutter={16} style={{margin: "10px"}}>
              <Col xs={24} sm={24} md={24} lg={24}>
                <div id="chartdiv" style={{ width: "100%", height: "500px" }}></div>
              </Col>
            </Row>
          </div>
        </div>
      </PageHeader>

    );
  }
}

export default ActiveAccountsChart;