import React, { Component } from 'react';
import { Network } from 'vis-network';
import { DataSet } from 'vis-data';

import {
  PageHeader, Row, Col, Spin, Form, Button, Input,
  DatePicker, InputNumber, Select,
} from 'antd';

import * as qs from 'query-string';
import api from '../../apis/blurtopianAPI';
import moment from 'moment';
import _ from 'lodash';

import 'antd/dist/antd.css';
import './Home.css';

const { RangePicker } = DatePicker;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

class VotesMapVisV2 extends Component {

  constructor(props) {
    super(props)
    this.state = {
      account: '',
      accounts: [],
      start_date: '',
      end_date: '',
      depth: 0,
      l0_account_votes: [],
      l1_account_votes: [],
      l2_account_votes: [],
      l3_account_votes: [],
      loading: false,
    }
  }

  componentWillUnmount() {
    if (this.chart) {
      this.chart.dispose();
    }
  }

  createChart = async () => {
    let {
      l0_account_votes,
      l1_account_votes,
      l2_account_votes,
//      l3_account_votes,
      depth,
    } = this.state;
    console.log('l0_account_votes', l0_account_votes)

    // create an array with edges
    let edge_data = [];
    if (depth >= 1) {
      l0_account_votes.forEach(item => {
        let { voter, author } = item._id;
        edge_data.push({
          from: voter,
          to: author,
          arrows: { to: true }
        });
      });
    }

    if (depth >= 2){
      l1_account_votes.forEach(item => {
        let { voter, author } = item._id;
        edge_data.push({
          from: voter,
          to: author,
          arrows: { to: true }
        });
      });
    }

    if (depth >= 3){
      l2_account_votes.forEach(item => {
        let { voter, author } = item._id;
        edge_data.push({
          from: voter,
          to: author,
          arrows: { to: true }
        });
      });
    }
    edge_data = edge_data.forEach(item => {

    })

    var edges = new DataSet(edge_data);

    let node_data = [];
    l0_account_votes.forEach(item => {
      let { author } = item._id;
      node_data.push({ id: author, label: author, value: item.count });
    });
    console.log('node_data', node_data)
    var nodes = new DataSet(node_data);

    // create a network
    var container = document.getElementById("chartdiv");
    var data = {
      nodes: nodes,
      edges: edges,
    };
    var options = {

      "edges": {
        "smooth": false
      },

      "physics": {
        "hierarchicalRepulsion": {
          "centralGravity": 0,
          "springConstant": 0
        },
        "minVelocity": 0.1,
        "maxVelocity": 1.5,
        "solver": "hierarchicalRepulsion"
      },

      interaction: {
        hover: true
      },

      nodes: {
        shape: 'dot',
        scaling: {
          customScalingFunction: function (min, max, total, value) {
            return value / total;
          },
          min: 10,
          max: 100
        }
      }
    };
    new Network(container, data, options);

  }

  getData = async (query) => {
    console.log('query', query)
    const response = await api.getUrl(`/api/votes/outvotes?${qs.stringify(query)}`);
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  handleFilter = () => {
    let {
      account, start_date, end_date, depth,
    } = this.state;
    start_date = start_date ? start_date : moment().subtract(7,'d').format("YYYY-MM-DD");
    end_date = end_date ? end_date : moment().format("YYYY-MM-DD");
    this.getData({
      voters: account,
      start_date,
      end_date,
    })
      .then(res => {
        this.setState({ l0_account_votes: res.data, loading: false })
        let l1_accounts = [];
        res.data.forEach(item => {
          l1_accounts.push(item._id.author)
        });
        console.log('l1_accounts', l1_accounts)
        this.getData({
          voters: l1_accounts,
          start_date,
          end_date,
        }).then(res => {
          this.setState({ l1_account_votes: res.data, loading: false })
          if (depth > 1) {
            let l2_accounts = [];
            res.data.forEach(item => {
              l2_accounts.push(item._id.author)
            });
            console.log('l2_accounts', l2_accounts)
            this.getData({
              voters: l2_accounts,
              start_date,
              end_date,
            }).then(res => {
              this.setState({ l2_account_votes: res.data, loading: false })
              if (depth > 2) {
                let l3_accounts = [];
                res.data.forEach(item => {
                  l3_accounts.push(item._id.author)
                });
                console.log('l3_accounts', l3_accounts)
                this.getData({
                  voters: l3_accounts,
                  start_date,
                  end_date,
                }).then(res => {
                  this.setState({ l3_account_votes: res.data, loading: false })
                  this.createChart();
                })
              } else {
                this.createChart();
              }
            })
          } else {
            this.createChart();
          }
          
        })

        
      })
  }

  handleChange = (value) => {
    console.log(`selected ${value}`);
  }

  render() {
    const { loading, account, depth } = this.state
    const disable_filter = _.isEmpty(account) || depth === 0;
    if (loading) {
      return (
        <div className="wrap">
          <div className="extraContent">
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12} style={{ textAlign: "center" }}>
                <Spin size="large" />
              </Col>
            </Row>
          </div>
        </div>
      )
    }

    return (
      <PageHeader>
        <div className="wrap">
          <div className="extraContent">
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12}>
                <Form {...formItemLayout}>
                  <Form.Item label="Range">
                    <RangePicker
                      format={'YYYY-MM-DD'}
                      defaultValue={[moment().subtract(7,'d'), moment()]}
                      onChange={(date, dateString) => this.setState({ start_date: date[0], end_date: date[1]}) }
                    />
                  </Form.Item>
                  {false &&
                  <Form.Item label="Account">
                    <Select
                      mode="tags"
                      style={{ width: '100%' }}
                      placeholder="Accounts"
                      notFoundContent={null}
                      onChange={value => this.setState({ accounts: value })}
                    >
                    </Select>
                  </Form.Item>
                  }
                  <Form.Item label="Account">
                    <Input
                      onChange={e => this.setState({ account: e.target.value })}
                    />
                  </Form.Item>
                  <Form.Item label="Depth">
                    <InputNumber
                      onChange={value => this.setState({ depth: value })}
                    />
                  </Form.Item>
                  <Form.Item {...tailLayout}>
                    <Button
                      onClick={this.handleFilter}
                      disabled={disable_filter}
                    > Filter</Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
            <Row>
              <Col xs={24} sm={24} md={24} lg={24}>
                <div id="chartdiv" style={{ width: "100%", height: "500px" }}></div>
              </Col>
            </Row>
          </div>
        </div>
      </PageHeader>

    );
  }
}

export default VotesMapVisV2;