import React, { Component } from 'react';
import {
  PageHeader, Row, Col, Spin, DatePicker, Form,
  Table,
} from 'antd';

import moment from 'moment';

import 'antd/dist/antd.css';
import './Summary.css';

const { RangePicker } = DatePicker;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

const dataSource = [
  {
    key: '1',
    name: 'Mike',
    age: 32,
    address: '10 Downing Street',
  },
  {
    key: '2',
    name: 'John',
    age: 42,
    address: '10 Downing Street',
  },
];

const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Age',
    dataIndex: 'age',
    key: 'age',
  },
  {
    title: 'Address',
    dataIndex: 'address',
    key: 'address',
  },
];

class ExecutiveSummary extends Component {

  render() {
    const { loading, isDisplayChart } = this.state

    return (
      <PageHeader>
        <div className="wrap">
          <div className="extraContent">
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12}>
                <Form {...formItemLayout}>
                  <Form.Item label="Range:">
                    <RangePicker
                      format={'YYYY-MM-DD'}
                      defaultValue={[moment().subtract(7,'d'), moment()]}
                      onChange={(date, dateString) => this.setState({ start_date: dateString[0], end_date: dateString[1]}) }
                    />
                  </Form.Item>
                </Form>
              </Col>
            </Row>
            {loading &&
              <Row type="flex" justify="center">
                <Col xs={24} sm={24} md={24} lg={12} style={{ textAlign: "center" }}>
                  <Spin size="large" />
                </Col>
              </Row>
            }
            {!loading && !isDisplayChart &&
              <Row gutter={16} style={{margin: "10px"}}>
                <Col xs={24} sm={24} md={24} lg={24}>
                  <Table
                    columns={columns}
                    dataSource={dataSource}
                    pagination={false}
                  />
                </Col>
              </Row>
            }
          </div>
        </div>
      </PageHeader>
    );
  }
}

export default ExecutiveSummary;