import React, { Component } from 'react';
import PropTypes from "prop-types";
import { Route } from 'react-router-dom';
import { withRouter } from "react-router";
import { withTranslation } from 'react-i18next';
import { Button, Col, Layout, Row, Menu, Select, Icon, message } from 'antd';

import ReactGA from 'react-ga';

import { ClientFooter } from './common';
import { ClientHome } from './homes';
import {
  VotesChart, VotesByCount, VotesMapVisV1, VotesMapVisV2,
  VotesTable, CommentsChart, ActiveAccountsChart, AccountsCreatedChart,
  Richlist, ActiveAuthors,
} from './charts';
import { Tweets } from './blurtmob';
import { CurationAccountsRatio, CurationAccountsRatioV2 } from './curation_accounts';
import { ExecutiveSummary } from './summary';
import { Redirect } from './redirects';

import api from '../apis/blurtopianAPI';

import 'antd/dist/antd.css';
import './Wrapper.css';

ReactGA.initialize('UA-145670123-1');

const { Content, Header } = Layout;
const { Option } = Select;

class ClientWrapper extends Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  };

  constructor() {
		super();
		this.state = {
      lang: '',
      isSignedIn: false,
      isGoogleLogin: false,
    };
    this.goBack = this.goBack.bind(this);
    this.goHome = this.goHome.bind(this);
    this.changeLang = this.changeLang.bind(this);
  }

  onLoginSuccess = info => {
    api.fetchUrl(`/api/login`, {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
      },
      body: JSON.stringify({
        email: info.profileObj.email,
      }),
    })
    .then(async res => {
      if (res.status === 200) {
        message.success("Successfully logged in.");
        const body = await res.json();
        this.setState({
          isSignedIn: true,
          isGoogleLogin: true,
          userInfo: {
            account: body.account,
          },
          err: null
        })
      } else {
        const error = new Error(res.error);
        throw error;
      }
    })
    .catch(err => {
      console.error(err);
      message.error('Error logging in.');
    });
  }

  logoutUser = () => {
    this.setState({
      isSignedIn: false,
      isGoogleLogin: false,
    });
  }

  onLoginFailed = (err) => {
    console.log('login failed', err)
    this.setState({
      isSignedIn: false,
      error: err,
    })
  }

  onLogoutSuccess = () => {
    this.setState({
      isSignedIn: false,
    })
  }

  onLogoutFailed = (err) => {
    this.setState({
      isSignedIn: false,
      error: err,
    })
  }

  goHome() {
    this.props.history.push("/");
  }

  goBack() {
    this.props.history.goBack();
  }

  changeLang = value => {
    this.setState(prevState => ({ lang: value }));
    this.props.i18n.changeLanguage(value);
  };

  render() {
    const { isSignedIn } = this.state;
    const pathname = this.props.location.pathname;
    const { t } = this.props;
    return (
      <Layout style={{ minHeight: '100vh' }}>
        <Layout>
          <Header
            style={{
              position: 'fixed',
              zIndex: 1,
              width: '100%',
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center"
            }}
          >
            <Menu
              selectable={false}
              theme="dark"
              mode="horizontal"
              style={{ lineHeight: '20px' }}
            >
              <Menu.Item
                key="2"
                style={{ padding: '0' }}
              >
                {(pathname !== "/") &&
                  <Button
                    type="default"
                    size="large"
                    onClick={this.goHome}
                  >
                    <Icon type="home" />
                  </Button>
                }
                {(pathname !== "/" && isSignedIn) &&
                  <Button
                    type="default"
                    size="large"
                    onClick={this.goBack}
                    style={{ marginLeft: "5px" }}
                  >
                    {t("Back")}
                  </Button>
                }
              </Menu.Item>
            </Menu>
            <Menu
              selectable={false}
              theme="dark"
              mode="horizontal"
              style={{ lineHeight: '20px' }}
            >
              <Menu.Item key="1">
                <Select
                  onChange={this.changeLang}
                  defaultValue="en"
                  style={{ marginRight: "10px", display: "none" }}
                >
                  <Option key="en" value="en">English</Option>
                  <Option key="jp" value="jp">日本語</Option>
                </Select>
              </Menu.Item>
            </Menu>
          </Header>
          <Content style={{ padding: '0 50px', marginTop: 75 }}>
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={16}>
                <div style={{ padding: 24, background: '#fff', minHeight: 320 }}>
                  <Route exact path="/"
                    render={(props) =>
                      <ClientHome
                        {...props}
                        services={this.state.services}
                        userInfo={this.state.userInfo}
                      />
                    }
                  />

                  <Route exact path="/charts/votes"
                    render={(props) =>
                      <VotesChart
                        {...props}
                        services={this.state.services}
                        userInfo={this.state.userInfo}
                      />
                    }
                  />

                  <Route exact path="/charts/votes_by_count"
                    render={(props) =>
                      <VotesByCount
                        {...props}
                        services={this.state.services}
                        userInfo={this.state.userInfo}
                      />
                    }
                  />

                  <Route exact path="/votes/votes_map_vis_v1"
                    render={(props) =>
                      <VotesMapVisV1
                        {...props}
                        services={this.state.services}
                        userInfo={this.state.userInfo}
                      />
                    }
                  />

                  <Route exact path="/votes/votes_map_vis_v2"
                    render={(props) =>
                      <VotesMapVisV2
                        {...props}
                        services={this.state.services}
                        userInfo={this.state.userInfo}
                      />
                    }
                  />

                  <Route exact path="/votes/votes_table"
                    render={(props) =>
                      <VotesTable
                        {...props}
                        services={this.state.services}
                        userInfo={this.state.userInfo}
                      />
                    }
                  />

                  <Route exact path="/charts/comments"
                    render={(props) =>
                      <CommentsChart
                        {...props}
                        services={this.state.services}
                        userInfo={this.state.userInfo}
                      />
                    }
                  />

                  <Route exact path="/charts/accounts"
                    render={(props) =>
                      <ActiveAccountsChart
                        {...props}
                        services={this.state.services}
                        userInfo={this.state.userInfo}
                      />
                    }
                  />

                  <Route exact path="/charts/active_authors"
                    render={(props) =>
                      <ActiveAuthors
                        {...props}
                        services={this.state.services}
                        userInfo={this.state.userInfo}
                      />
                    }
                  />

                  <Route exact path="/accounts/created"
                    render={(props) =>
                      <AccountsCreatedChart
                        {...props}
                        services={this.state.services}
                        userInfo={this.state.userInfo}
                      />
                    }
                  />

                  <Route exact path="/curation_accounts/total_vests_ratio"
                    render={(props) =>
                      <CurationAccountsRatio
                        {...props}
                        services={this.state.services}
                        userInfo={this.state.userInfo}
                      />
                    }
                  />

                  <Route exact path="/curation_accounts/total_vests_ratio_v2"
                    render={(props) =>
                      <CurationAccountsRatioV2
                        {...props}
                        services={this.state.services}
                        userInfo={this.state.userInfo}
                      />
                    }
                  />

                  <Route exact path="/blurtmob/tweets"
                    render={(props) =>
                      <Tweets
                        {...props}
                        services={this.state.services}
                        userInfo={this.state.userInfo}
                      />
                    }
                  />

                  <Route exact path="/richlist"
                    render={(props) =>
                      <Richlist
                        {...props}
                        services={this.state.services}
                        userInfo={this.state.userInfo}
                      />
                    }
                  />

                  <Route exact path="/summaries/executive"
                    render={(props) =>
                      <ExecutiveSummary
                        {...props}
                        services={this.state.services}
                        userInfo={this.state.userInfo}
                      />
                    }
                  />

                  <Route exact path="/redirect"
                    render={(props) =>
                      <Redirect />
                    }
                  />
                </div>
              </Col>
            </Row>
          </Content>
          <ClientFooter />
        </Layout>
      </Layout>
    );
  }
}

export default withTranslation()(withRouter(ClientWrapper));