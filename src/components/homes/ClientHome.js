import React, { Component } from 'react';
import { Row, Col, Icon, Card, Badge } from 'antd';
import { NavLink } from 'react-router-dom';

import 'antd/dist/antd.css';
import './Home.css';

class ClientHome extends Component {
  render() {
    return (
        <div className="wrap">
          <div className="extraContent">
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={18}
                style={{ display: "flex", justifyContent: "center" }}
              >
                <h2>Blurt Business Intelligence</h2>
              </Col>
            </Row>
            <Row gutter={16} style={{margin: "10px"}}>
              <Col xs={24} sm={24} md={24} lg={8}>
                <Card style={{display: "flex", justifyContent: "center"}}>
                  <NavLink to="/charts/votes">
                    <Icon type="up-circle" style={{ fontSize: '4em', color: '#08c' }}/>
                  </NavLink>
                </Card>
              </Col>
              <Col xs={24} sm={24} md={24} lg={8}>
                <Card style={{display: "flex", justifyContent: "center"}}>
                  <NavLink to="/charts/votes_by_count">
                    <Badge count={"By Count"}>
                      <Icon type="up-circle" style={{ fontSize: '4em', color: '#08c' }}/>
                    </Badge>
                  </NavLink>
                </Card>
              </Col>
              <Col xs={24} sm={24} md={24} lg={8}>
                <Card style={{display: "flex", justifyContent: "center"}}>
                  <NavLink to="/charts/comments">
                    <Icon type="edit" style={{ fontSize: '4em', color: '#08c' }}/>
                  </NavLink>
                </Card>
              </Col>
              <Col xs={24} sm={24} md={24} lg={8}>
                <Card style={{display: "flex", justifyContent: "center"}}>
                  <NavLink to="/votes/votes_map_vis_v1">
                    <Badge count={<Icon type="up-circle"/>}>
                      <Icon type="dot-chart" style={{ fontSize: '4em', color: '#08c' }}/>
                    </Badge>
                  </NavLink>
                </Card>
              </Col>
              {false &&
              <Col xs={24} sm={24} md={24} lg={8}>
                <Card style={{display: "flex", justifyContent: "center"}}>
                  <NavLink to="/votes/votes_map_vis_v2">
                    <Badge count={<Icon type="up-circle"/>}>
                      <Icon type="dot-chart" style={{ fontSize: '4em', color: '#08c' }}/>
                    </Badge>
                  </NavLink>
                </Card>
              </Col>
              }
              <Col xs={24} sm={24} md={24} lg={8}>
                <Card style={{display: "flex", justifyContent: "center"}}>
                  <NavLink to="/votes/votes_table">
                    <Badge count={<Icon type="up-circle"/>}>
                      <Icon type="table" style={{ fontSize: '4em', color: '#08c' }}/>
                    </Badge>
                  </NavLink>
                </Card>
              </Col>
              <Col xs={24} sm={24} md={24} lg={8}>
                <Card style={{display: "flex", justifyContent: "center"}}>
                  <NavLink to="/accounts/created">
                    <Icon type="team" style={{ fontSize: '4em', color: '#08c' }}/>
                  </NavLink>
                </Card>
              </Col>
              <Col xs={24} sm={24} md={24} lg={8}>
                <Card style={{display: "flex", justifyContent: "center"}}>
                  <NavLink to="/blurtmob/tweets">
                    <Icon type="twitter" style={{ fontSize: '4em', color: '#08c' }}/>
                  </NavLink>
                </Card>
              </Col>
              <Col xs={24} sm={24} md={24} lg={8}>
                <Card style={{display: "flex", justifyContent: "center"}}>
                  <NavLink to="/curation_accounts/total_vests_ratio_v2">
                    <Icon type="pie-chart" style={{ fontSize: '4em', color: '#08c' }}/>
                  </NavLink>
                </Card>
              </Col>
              <Col xs={24} sm={24} md={24} lg={8}>
                <Card style={{display: "flex", justifyContent: "center"}}>
                  <NavLink to="/richlist">
                    <Icon type="crown" style={{ fontSize: '4em', color: '#08c' }}/>
                  </NavLink>
                </Card>
              </Col>
              <Col xs={24} sm={24} md={24} lg={8}>
                <Card style={{display: "flex", justifyContent: "center"}}>
                  <NavLink to="/graphene_chains">
                    <Icon type="dashboard" style={{ fontSize: '4em', color: '#08c' }}/>
                  </NavLink>
                </Card>
              </Col>
              <Col xs={24} sm={24} md={24} lg={8}>
                <Card style={{display: "flex", justifyContent: "center"}}>
                  <NavLink to="/curation_accounts/reports">
                    <Icon type="deployment-unit" style={{ fontSize: '4em', color: '#08c' }}/>
                  </NavLink>
                </Card>
              </Col>
              <Col xs={24} sm={24} md={24} lg={8}>
                <Card style={{display: "flex", justifyContent: "center"}}>
                  <NavLink to="/summaries/reports">
                    <Icon type="audit" style={{ fontSize: '4em', color: '#08c' }}/>
                  </NavLink>
                </Card>
              </Col>
            </Row>
          </div>
        </div>

    );
  }
}

export default ClientHome;