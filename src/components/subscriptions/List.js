import React, { Component } from 'react';
import PropTypes from "prop-types";
import { NavLink } from 'react-router-dom';
import { withRouter } from "react-router";
import {
  Col, Icon, Row, Spin, Table, Button, message,
} from 'antd';

import api from '../../apis/blurtopianAPI';

import 'antd/dist/antd.css';
import './List.css';

class Subscriptions extends Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      items: [],
      loading: false,
    };
  }

  createTableColumns = async () => {
    this.columns = [
      {
        title: 'No',
        dataIndex: 'rowKey',
        key: 'roKey._id',
        render: rowKey =>
          <NavLink
            style={{ padding: 10 }}
            to={`/projects/${rowKey._id}/edit`}
          >
            <Icon type={"edit"}/>
          </NavLink>,
      },
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        render: name => <span>{name}</span>,
      },
      {
        title: 'Description',
        dataIndex: 'description',
        key: 'description',
        render: description => <span>{description}</span>,
      },
      {
        title: 'Is Active',
        dataIndex: 'isActive',
        key: 'isActive',
        render: isActive => <span>{isActive ? 'Y' : 'N'}</span>,
      },
      {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <span>
            <NavLink
              to={`/projects/${record._id}/edit`}
            >
              <Button block type="link">
                <Icon type={"edit"}/>
              </Button>
            </NavLink>
            <NavLink
              to={`/projects/${record._id}/history`}
            >
              <Button block type="link">
                <Icon type={"audit"}/>
              </Button>
            </NavLink>
            <NavLink
              to={`/projects/${record._id}/reward_labels`}
            >
              <Button block type="link">
                <Icon type={"tags"}/>
              </Button>
            </NavLink>
          </span>
        ),
      },
    ];
  }

  componentDidMount() {
    this.getRequiredInfoFromAPI();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.location !== this.props.location) {
      this.getRequiredInfoFromAPI();
    }
  }

  getRequiredInfoFromAPI = async () => {
    this.setState({ loading: true });
    this.getItems()
      .then(res => {
        this.setState({ items: res.data, loading: false })
      })
  }

  getItems = async () => {
    const response = await api.getUrl(`/api/projects`)
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  handleDelete = async (_id) => {
    await api.fetchUrl(`/api/projects/${_id}`, {
      method: 'DELETE',
    });
    message.success("Project successfully removed.")
    this.props.history.push('/projects');
  };

  render() {
    this.createTableColumns();
    const { items, loading } = this.state;
    let modResult = [];
    let i = 0;
    items.forEach(item => {
      i++;
      modResult.push({ ...item, key: i, rowKey: { _id: item._id, rowNum: i } });
    });

    return (
      <div className="wrap">
        <div className="extraContent">
          {loading ?
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12} style={{ textAlign: "center" }}>
                <Spin size="large" />
              </Col>
            </Row>
          :
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12}>
              {(items.length === 0) ?
                <div>
                  <h3>{`Sorry, but there are no projects registered.`}</h3>
                  <NavLink to="/projects/new">
                    <Icon type="plus"/>Add Project
                  </NavLink>
                </div>
              :
                <div>
                  <NavLink to="/projects/new">
                    <Icon type="plus"/>Add Project
                  </NavLink>
                  <h3>{`Here are the projects available:`}</h3>
                  <Table
                    columns={this.columns}
                    dataSource={modResult}
                    pagination={{hideOnSinglePage: true}}
                  />
                </div>
              }
              </Col>
            </Row>
          }
        </div>
      </div>
    );
  }
}

export default withRouter(Subscriptions);
