import React, { Component } from 'react';
import {
  Button, Col, Form, Row, Input, InputNumber, PageHeader, message, Switch,
} from 'antd';
import ReactGA from 'react-ga';
import _ from "lodash";

import api from '../../apis/blurtopianAPI';

import 'antd/dist/antd.css';
import './Home.css';

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

class LabelReward extends Component {
  state = {
    isLabelInputted: false,
    isRewardInputted: false,
  }
  render() {
    const { index } = this.props;
    const { isLabelInputted, isRewardInputted } = this.state;

    return (
      <Row type="flex" justify="space-around">
        <Col xs={24} sm={24} md={24} lg={6}>
          <Form.Item
            validateStatus={isRewardInputted && !isLabelInputted ? "error" : ""}
            help={isRewardInputted && !isLabelInputted ? "Please input label" : ""}
          >
            <Input
              onChange={e => {
                const isLabelInputted = !_.isEmpty(e.target.value);
                this.setState({ isLabelInputted });
                this.props.handleLabelInput(e.target.value, index);
              }}
              style={{ width: 145 }}
            />
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={24} lg={8}>
          <Form.Item
            validateStatus={isLabelInputted && !isRewardInputted ? "error" : ""}
            help={isLabelInputted && !isRewardInputted ? "Please input reward" : ""}
          >
            <InputNumber
              onChange={value => {
                const isRewardInputted = value > 0;
                console.log('isRewardInputted', isRewardInputted)
                this.setState({ isRewardInputted });
                this.props.handleRewardInput(value, index);
              }}
              style={{ width: 100 }}
            />
          </Form.Item>
        </Col>
      </Row>
    );
  }
}

class NewSubscription extends Component {
  state = {
    record: {},
    loading: false,
    submitting: false,
    labelRewards: {},
  };

  updateRecord = async (updatedInfo) => {
    const currentInfo = this.state.record;
    this.setState({
      record: { ...currentInfo, ...updatedInfo }
    });
  };

  handleSubmit = async (e) => {
    ReactGA.event({
      category: 'Button Click',
      action: 'submit new bank account'
    });

    e.preventDefault();
    const { record, labelRewards } = this.state;
    this.setState({ submitting: true });
    api.fetchUrl(`/api/projects`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
      },
      body: JSON.stringify(
        { ...record, labelRewards }
      ),
    })
    .then(async res => {
      if (res.status === 200) {
        const response = await res.json();
        if (!response.error_id) {
          message.success('Successfully added.');
          this.setState({ submitting: false });
          this.props.history.push(`/projects`);
        }
      } else {
        const error = new Error(res.error);
        throw error;
      }
    })
    .catch(err => {
      console.error(err);
      this.setState({ submitting: false });
      message.error('Error submitting.');
    });
  };

  handleLabelInput = (value, index) => {
    const { labelRewards } = this.state;
    if (_.isEmpty(labelRewards[index])) {
      labelRewards[index] = {};
    }
    labelRewards[index] = {
      reward: labelRewards[index].reward,
      label: value,
    };
    this.setState({ labelRewards });
  };

  handleRewardInput = (value, index) => {
    const { labelRewards } = this.state;
    if (_.isEmpty(labelRewards[index])) {
      labelRewards[index] = {};
    }
    labelRewards[index] = {
      label: labelRewards[index].label,
      reward: value,
    };
    this.setState({ labelRewards });
  };

  render() {
    const { record } = this.state;
    const { name, description } = record;
    const disableSubmit = !name || !description;

    return (
      <PageHeader>
        <div className="wrap">
          <div className="extraContent">
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12}>
                <Form {...formItemLayout}>
                  <Form.Item label="Name:">
                    <Input
                      onChange={e => this.updateRecord({ name: e.target.value })}
                    />
                  </Form.Item>
                  <Form.Item label="Description:">
                    <Input
                      onChange={e => this.updateRecord({ description: e.target.value })}
                    />
                  </Form.Item>
                  <Form.Item label="Is active?">
                    <Switch
                      checkedChildren="Yes"
                      unCheckedChildren="No"
                      onChange={(checked) => this.updateRecord({ isActive: checked })}
                    />
                  </Form.Item>
                </Form>
              </Col>
            </Row>
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={18}
                style={{ display: "flex", justifyContent: "center" }}
              >
                <h3>Project Reward-Labels</h3>
              </Col>
            </Row>
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12}>
                <Form layout="inline">
                  <LabelReward
                    handleLabelInput={this.handleLabelInput}
                    handleRewardInput={this.handleRewardInput}
                    index={0}
                    key={0}
                  />
                  <LabelReward
                    handleLabelInput={this.handleLabelInput}
                    handleRewardInput={this.handleRewardInput}
                    index={1}
                    key={1}
                  />
                </Form>
              </Col>
            </Row>
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12}>
                <Form {...formItemLayout}>
                  <Form.Item {...tailFormItemLayout}>
                    <Button block type="primary"
                      loading={this.state.submitting}
                      onClick={this.handleSubmit}
                      disabled={disableSubmit}
                    >
                      {"Submit"}
                    </Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          </div>
        </div>
      </PageHeader>
    );
  }
}

export default NewSubscription;