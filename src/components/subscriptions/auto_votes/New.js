import React, { Component } from 'react';
import {
  Button, Col, Form, Row, Input, PageHeader, message, Switch, InputNumber, Select,
} from 'antd';
import ReactGA from 'react-ga';

import api from '../../../apis/blurtopianAPI';

import 'antd/dist/antd.css';
import './Home.css';

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

class NewAutoVote extends Component {
  state = {
    record: {},
    loading: false,
    submitting: false,
    labelRewards: {},
  };

  updateRecord = async (updatedInfo) => {
    const currentInfo = this.state.record;
    this.setState({
      record: { ...currentInfo, ...updatedInfo }
    });
  };

  handleSubmit = async (e) => {
    ReactGA.event({
      category: 'Button Click',
      action: 'submit new auto vote setting'
    });

    e.preventDefault();
    const { subscription_id } = this.props.match.params;
    const { record } = this.state;
    this.setState({ submitting: true });
    api.fetchUrl(`/api/subscriptions/${subscription_id}/auto_votes`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
      },
      body: JSON.stringify(
        { ...record }
      ),
    })
    .then(async res => {
      if (res.status === 200) {
        const response = await res.json();
        if (!response.error_id) {
          message.success('Successfully added.');
          this.setState({ submitting: false });
          this.props.history.push(`/subscriptions/${subscription_id}/auto_votes`);
        }
      } else {
        const error = new Error(res.error);
        throw error;
      }
    })
    .catch(err => {
      console.error(err);
      this.setState({ submitting: false });
      message.error('Error submitting.');
    });
  };

  render() {
    const { record } = this.state;
    const {
      account, vote_weight, delay, daily_limit, weekly_limit
    } = record;
    const disableSubmit = !account || !vote_weight || !delay || !daily_limit || !weekly_limit;

    return (
      <PageHeader>
        <div className="wrap">
          <div className="extraContent">
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12}>
                <Form {...formItemLayout}>
                <Form.Item label="Account:">
                    <Input
                      onChange={e => this.updateRecord({ account: e.target.value })}
                    />
                  </Form.Item>
                  <Form.Item label="Vote Weight:">
                    <InputNumber
                      onChange={value => this.updateRecord({ vote_weight: value })}
                      max={10000}
                    />
                  </Form.Item>
                  <Form.Item label="Delay:">
                    <InputNumber
                      onChange={value => this.updateRecord({ delay: value })}
                    />
                  </Form.Item>
                  <Form.Item label="Daily Limit:">
                    <InputNumber
                      onChange={value => this.updateRecord({ daily_limit: value })}
                    />
                  </Form.Item>
                  <Form.Item label="Weekly Limit:">
                    <InputNumber
                      onChange={value => this.updateRecord({ weekly_limit: value })}
                    />
                  </Form.Item>
                  <Form.Item label="Tags:">
                    <Select
                      mode="tags"
                      style={{ width: '100%' }}
                      onChange={value => this.updateRecord({ tags: value })}
                      tokenSeparators={[' ', ',']}
                      notFoundContent={null}
                    >
                    </Select>
                  </Form.Item>
                  <Form.Item label="Is active?">
                    <Switch
                      checkedChildren="Yes"
                      unCheckedChildren="No"
                      onChange={(checked) => this.updateRecord({ is_active: checked })}
                    />
                  </Form.Item>
                  <Form.Item {...tailFormItemLayout}>
                    <Button block type="primary"
                      loading={this.state.submitting}
                      onClick={this.handleSubmit}
                      disabled={disableSubmit}
                    >
                      {"Submit"}
                    </Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          </div>
        </div>
      </PageHeader>
    );
  }
}

export default NewAutoVote;