import React, { Component } from 'react';
import PropTypes from "prop-types";
import { withRouter } from "react-router";
import {
  Button, Form, Row, Col, PageHeader, Spin,
  message, Input, Switch, InputNumber, Select,
} from 'antd';
import ReactGA from 'react-ga';

import api from '../../../apis/blurtopianAPI';

import 'antd/dist/antd.css';
import './CreateForm.css';

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 14,
      offset: 10,
    },
  },
};

class EditAutoVote extends Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    userInfo: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props)
    this.state = {
      record: {},
      loadingRecord: true,
      labelRewards: {},
      labelRewardsList: [],
    }

    this.updateRecord = this.updateRecord.bind(this);
  }
  componentDidMount() {
    this.getRequiredInfoFromAPI();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.location !== this.props.location) {
      this.getRequiredInfoFromAPI();
    }
  }

  getRequiredInfoFromAPI = async () => {
    this.setState({ loadingRecord: true });
    this.getRecord()
      .then(res => {
        this.setState({ record: res.data, loadingRecord: false });
      })
  }

  getRecord = async () => {
    const { subscription_id, _id } = this.props.match.params;
    const response = await api.getUrl(`/api/subscriptions/${subscription_id}/auto_votes/${_id}`);
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  updateRecord = async (updatedInfo) => {
    const currentInfo = this.state.record;
    this.setState({
      record: { ...currentInfo, ...updatedInfo }
    });
  };

  handleSubmit = async (e) => {
    ReactGA.event({
      category: 'Button Click',
      action: 'edit subscription'
    });

    e.preventDefault();
    const { record } = this.state;
    const { subscription_id, _id } = this.props.match.params;
    const { userMemberId } = this.props.userInfo;

    this.setState({ submitting: true });
    api.fetchUrl(`/api/subscriptions/${subscription_id}/auto_votes/${_id}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
      },
      body: JSON.stringify({
        ...record,
        userMemberId,
      }),
    })
    .then(async res => {
      if (res.status === 200) {
        const response = await res.json();
        if (!response.error_id) {
          message.success('Successfully added.');
          this.setState({ submitting: false });
          this.props.history.push(`/subscriptions/${subscription_id}/auto_votes`);
        }
      } else {
        const error = new Error(res.error);
        throw error;
      }
    })
    .catch(err => {
      console.error(err);
      this.setState({ submitting: false });
      message.error('Error submitting.');
    });
  };

  render() {
    const { record, loadingRecord } = this.state;
    const {
      account, vote_weight, delay, daily_limit, weekly_limit, tags, is_active
    } = record;
    const disableSubmit = !account || !vote_weight || !delay || !daily_limit || !weekly_limit;
    const loading =  loadingRecord;
    if (loading) {
      return (
        <div className="wrap">
          <div className="extraContent">
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12} style={{ textAlign: "center" }}>
                <Spin size="large" />
              </Col>
            </Row>
          </div>
        </div>
      )
    }

    return (
      <PageHeader>
        <div className="wrap">
          <div className="extraContent">
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12}>
                <Form {...formItemLayout}>
                  <Form.Item label="Account:">
                    <Input
                      onChange={e => this.updateRecord({ account: e.target.value })}
                      defaultValue={account}
                    />
                  </Form.Item>
                  <Form.Item label="Vote Weight:">
                    <InputNumber
                      onChange={value => this.updateRecord({ vote_weight: value })}
                      max={10000}
                      defaultValue={vote_weight}
                    />
                  </Form.Item>
                  <Form.Item label="Delay:">
                    <InputNumber
                      onChange={value => this.updateRecord({ delay: value })}
                      defaultValue={delay}
                    />
                  </Form.Item>
                  <Form.Item label="Daily Limit:">
                    <InputNumber
                      onChange={value => this.updateRecord({ daily_limit: value })}
                      defaultValue={daily_limit}
                    />
                  </Form.Item>
                  <Form.Item label="Weekly Limit:">
                    <InputNumber
                      onChange={value => this.updateRecord({ weekly_limit: value })}
                      defaultValue={weekly_limit}
                    />
                  </Form.Item>
                  <Form.Item label="Tags:">
                    <Select
                      mode="tags"
                      style={{ width: '100%' }}
                      onChange={value => this.updateRecord({ tags: value })}
                      tokenSeparators={[' ', ',']}
                      notFoundContent={null}
                      defaultValue={tags}
                    >
                      {tags.map(item => {
                        return <Select.Option key={item} value={item}>{item}</Select.Option>
                      })}
                    </Select>
                  </Form.Item>
                  <Form.Item label="Is active?">
                    <Switch
                      checkedChildren="Yes"
                      unCheckedChildren="No"
                      onChange={(checked) => this.updateRecord({ is_active: checked })}
                      defaultChecked={is_active}
                    />
                  </Form.Item>
                  <Form.Item {...tailFormItemLayout}>
                    <Button block type="primary"
                      loading={this.state.submitting}
                      onClick={this.handleSubmit}
                      disabled={disableSubmit}
                    >
                      {"Submit"}
                    </Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          </div>
        </div>
      </PageHeader>
    );
  }
}

export default withRouter(EditAutoVote);