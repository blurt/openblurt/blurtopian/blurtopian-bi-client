import React, { Component } from 'react';
import PropTypes from "prop-types";
import { NavLink } from 'react-router-dom';
import { withRouter } from "react-router";
import {
  Col, Icon, Row, Spin, Table, Button, message, Tag,
} from 'antd';

import api from '../../../apis/blurtopianAPI';

import 'antd/dist/antd.css';
import './List.css';

class AutoVotes extends Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      items: [],
      loading: false,
    };
  }

  createTableColumns = async (subscription_id) => {
    this.columns = [
      {
        title: 'No',
        dataIndex: 'rowKey',
        key: 'roKey._id',
        render: rowKey =>
          <NavLink
            style={{ padding: 10 }}
            to={`/subscriptions/${subscription_id}/auto_votes/${rowKey._id}/edit`}
          >
            <Icon type={"edit"}/>
          </NavLink>,
      },
      {
        title: 'Account',
        dataIndex: 'account',
        key: 'account',
        render: account => <span>{account}</span>,
      },
      {
        title: 'Vote Weight',
        dataIndex: 'vote_weight',
        key: 'vote_weight',
        render: vote_weight => <span>{vote_weight}</span>,
      },
      {
        title: 'Delay',
        dataIndex: 'delay',
        key: 'delay',
        render: delay => <span>{delay}</span>,
      },
      {
        title: 'Limits',
        key: 'limits',
        render: (text, record) => (
          <span>
            D: {record.daily_limit} / W: {record.weekly_limit} 
          </span>
        ),
      },
      {
        title: 'Tags',
        key: 'tags',
        render: (text, record) => (
          <span>
            {record.tags.map(item => {
              return <Tag>{item}</Tag>;
            })}
          </span>
        ),
      },
      {
        title: 'Is Active',
        dataIndex: 'is_active',
        key: 'is_active',
        render: is_active => <span>{is_active ? 'Y' : 'N'}</span>,
      },
      {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <span>
            <NavLink
              to={`/subscriptions/${subscription_id}/auto_votes/${record._id}/edit`}
            >
              <Button block type="link">
                <Icon type={"edit"}/>
              </Button>
            </NavLink>
          </span>
        ),
      },
    ];
  }

  componentDidMount() {
    this.getRequiredInfoFromAPI();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.location !== this.props.location) {
      this.getRequiredInfoFromAPI();
    }
  }

  getRequiredInfoFromAPI = async () => {
    this.setState({ loading: true });
    this.getItems()
      .then(res => {
        this.setState({ items: res.data, loading: false })
      })
  }

  getItems = async () => {
    const { subscription_id } = this.props.match.params;
    const response = await api.getUrl(`/api/subscriptions/${subscription_id}/auto_votes`)
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  handleDelete = async (_id) => {
    const { subscription_id } = this.props.match.params;
    await api.fetchUrl(`/api/subscriptions/${subscription_id}/auto_votes/${_id}`, {
      method: 'DELETE',
    });
    message.success("Reward label successfully removed.")
    this.props.history.push(`/subscriptions/${subscription_id}`);
  };

  render() {
    const { subscription_id } = this.props.match.params;
    this.createTableColumns(subscription_id);
    const { items, loading } = this.state;
    let modResult = [];
    let i = 0;
    items.forEach(item => {
      i++;
      modResult.push({ ...item, key: i, rowKey: { _id: item._id, rowNum: i } });
    });

    return (
      <div className="wrap">
        <div className="extraContent">
          {loading ?
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12} style={{ textAlign: "center" }}>
                <Spin size="large" />
              </Col>
            </Row>
          :
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={24}>
              {(items.length === 0) ?
                <div>
                  <h3>{`Sorry, but there are no auto-vote settings registered.`}</h3>
                  <NavLink to={`/subscriptions/${subscription_id}/auto_votes/new`}>
                    <Icon type="plus"/>Add Auto-Vote Setting
                  </NavLink>
                </div>
              :
                <div>
                  <NavLink to={`/subscriptions/${subscription_id}/auto_votes/new`}>
                    <Icon type="plus"/>Add Auto-Vote Setting
                  </NavLink>
                  <h3>{`Here are the auto-vote settings available:`}</h3>
                  <Table
                    columns={this.columns}
                    dataSource={modResult}
                    pagination={{hideOnSinglePage: true}}
                  />
                </div>
              }
              </Col>
            </Row>
          }
        </div>
      </div>
    );
  }
}

export default withRouter(AutoVotes);
