export { default as AutoVotes } from './List';
export { default as EditAutoVote } from './Edit';
export { default as NewAutoVote } from './New';