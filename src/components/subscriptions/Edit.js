import React, { Component } from 'react';
import PropTypes from "prop-types";
import { NavLink } from 'react-router-dom';
import { withRouter } from "react-router";
import {
  Button, Form, Row, Col, PageHeader, Spin,
  message, Input, Switch, InputNumber, Icon,
} from 'antd';
import ReactGA from 'react-ga';
import _ from "lodash";

import api from '../../apis/blurtopianAPI';

import 'antd/dist/antd.css';
import './CreateForm.css';

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 14,
      offset: 10,
    },
  },
};

class LabelReward extends Component {
  state = {
    isLabelInputted: false,
    isRewardInputted: false,
  }

  componentDidMount() {
    let { localeLink } = this.props;
    if (!_.isEmpty(localeLink)) {
      if (localeLink.localeChurch) this.setState({ isLocaleChurchInputted: true });
      if (localeLink.link) this.setState({ isLinkInputted: true });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.location !== this.props.location) {
      let { localeLink } = this.props;
      if (!_.isEmpty(localeLink)) {
        if (localeLink.localeChurch) this.setState({ isLocaleChurchInputted: true });
        if (localeLink.link) this.setState({ isLinkInputted: true });
      }
    }
  }

  render() {
    const { index } = this.props;
    const { isLabelInputted, isRewardInputted } = this.state;

    return (
      <Row type="flex" justify="space-around">
        <Col xs={24} sm={24} md={24} lg={6}>
          <Form.Item
            validateStatus={isRewardInputted && !isLabelInputted ? "error" : ""}
            help={isRewardInputted && !isLabelInputted ? "Please input label" : ""}
          >
            <Input
              onChange={e => {
                const isLabelInputted = !_.isEmpty(e.target.value);
                this.setState({ isLabelInputted });
                this.props.handleLabelInput(e.target.value, index);
              }}
              style={{ width: 145 }}
            />
          </Form.Item>
        </Col>
        <Col xs={24} sm={24} md={24} lg={8}>
          <Form.Item
            validateStatus={isLabelInputted && !isRewardInputted ? "error" : ""}
            help={isLabelInputted && !isRewardInputted ? "Please input reward" : ""}
          >
            <InputNumber
              onChange={value => {
                const isRewardInputted = value > 0;
                console.log('isRewardInputted', isRewardInputted)
                this.setState({ isRewardInputted });
                this.props.handleRewardInput(value, index);
              }}
              style={{ width: 100 }}
            />
          </Form.Item>
        </Col>
      </Row>
    );
  }
}

class EditSubscription extends Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    userInfo: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props)
    this.state = {
      record: {},
      loadingRecord: true,
      labelRewards: {},
      labelRewardsList: [],
    }

    this.updateRecord = this.updateRecord.bind(this);
  }
  componentDidMount() {
    this.getRequiredInfoFromAPI();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.location !== this.props.location) {
      this.getRequiredInfoFromAPI();
    }
  }

  getRequiredInfoFromAPI = async () => {
    this.setState({ loadingRecord: true });
    this.getRecord()
      .then(res => {
        const labelRewardsList = res.data.labelRewards;
        this.setState({ record: res.data, loadingRecord: false });
        let labelRewards = {};
        labelRewardsList.forEach((item, index) => {
          const { label, reward } = item;
          labelRewards[index] = { label, reward }
        });
        this.setState({
          labelRewards,
          labelRewardsList,
        });
      })
  }

  getRecord = async () => {
    const { _id } = this.props.match.params;
    const response = await api.getUrl(`/api/projects/${_id}`);
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  updateRecord = async (updatedInfo) => {
    const currentInfo = this.state.record;
    this.setState({
      record: { ...currentInfo, ...updatedInfo }
    });
  };

  handleSubmit = async (e) => {
    ReactGA.event({
      category: 'Button Click',
      action: 'edit project'
    });

    e.preventDefault();
    const { record, labelRewards } = this.state;
    const { _id } = this.props.match.params;
    const { userMemberId } = this.props.userInfo;

    this.setState({ submitting: true });
    api.fetchUrl(`/api/projects/${_id}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
      },
      body: JSON.stringify({
        ...record,
        userMemberId,
        labelRewards,
      }),
    })
    .then(async res => {
      if (res.status === 200) {
        const response = await res.json();
        if (!response.error_id) {
          message.success('Successfully added.');
          this.setState({ submitting: false });
          this.props.history.push(`/projects`);
        }
      } else {
        const error = new Error(res.error);
        throw error;
      }
    })
    .catch(err => {
      console.error(err);
      this.setState({ submitting: false });
      message.error('Error submitting.');
    });
  };

  render() {
    const { _id } = this.props.match.params;
    const { record, loadingRecord, labelRewardsList } = this.state;
    const {
      name, description, isActive,
    } = record;
    const disableSubmit = !name || !description;
    const loading =  loadingRecord;
    if (loading) {
      return (
        <div className="wrap">
          <div className="extraContent">
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12} style={{ textAlign: "center" }}>
                <Spin size="large" />
              </Col>
            </Row>
          </div>
        </div>
      )
    }

    return (
      <PageHeader>
        <div className="wrap">
          <div className="extraContent">
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12}>
                <Form {...formItemLayout}>
                  <Form.Item label="Name:">
                    <Input
                      onChange={e => this.updateRecord({ name: e.target.value })}
                      defaultValue={name}
                    />
                  </Form.Item>
                  <Form.Item label="Description:">
                    <Input
                      onChange={e => this.updateRecord({ description: e.target.value })}
                      defaultValue={description}
                    />
                  </Form.Item>
                  <Form.Item label="Is active?">
                    <Switch
                      checkedChildren="Yes"
                      unCheckedChildren="No"
                      defaultChecked={isActive}
                      onChange={(checked) => this.updateRecord({ isActive: checked })}
                    />
                  </Form.Item>
                </Form>
              </Col>
            </Row>
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={18}
                style={{ display: "flex", justifyContent: "center" }}
              >
                <h3>Project Reward Labels</h3>
                <NavLink to={`/projects/${_id}/reward_labels/new`}>
                  <Icon type="plus"/>Add Reward Label
                </NavLink>
              </Col>
            </Row>
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12}>
                <Form layout="inline">
                  {_.range(labelRewardsList.length).map(i => {
                    return (
                      <LabelReward
                        handleLabelInput={this.handleLabelInput}
                        handleRewardInput={this.handleRewardInput}
                        labelReward={labelRewardsList[i]}
                        index={i}
                        key={i}
                      />
                    )
                  })}
                </Form>
              </Col>
            </Row>
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={12}>
                <Form {...formItemLayout}>
                  <Form.Item {...tailFormItemLayout}>
                    <Button block type="primary"
                      loading={this.state.submitting}
                      onClick={this.handleSubmit}
                      disabled={disableSubmit}
                    >
                      {"Submit"}
                    </Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          </div>
        </div>
      </PageHeader>
    );
  }
}

export default withRouter(EditSubscription);