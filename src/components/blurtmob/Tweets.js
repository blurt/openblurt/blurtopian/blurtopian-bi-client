import React, { Component } from 'react';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

import {
  PageHeader, Row, Col, Spin, Form, Button,
  Switch, Table, Tag, Input, InputNumber, Checkbox,
} from 'antd';

import _ from 'lodash';
import * as qs from 'query-string';
import api from '../../apis/blurtopianAPI';

import 'antd/dist/antd.css';
import './Home.css';

am4core.useTheme(am4themes_animated);

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

class AccountsCreatedChart extends Component {

  constructor(props) {
    super(props)
    this.state = {
      parent_permlink: '',
      min_count: 1,
      isDisplayChart: true,
      useV2: false,
      items: [],
      loading: false,
    }
  }

  componentWillUnmount() {
    if (this.chart) {
      this.chart.dispose();
    }
  }

  createChart = () => {
    let { items } = this.state;
    let chart = am4core.create("chartdiv", am4charts.XYChart);
    chart.paddingRight = 20;

    let data = [];
    items.forEach(item => {
      data.push({ author: item._id , value: item.count });
    });

    chart.data = data;

    let title = chart.titles.create();
    title.text = "BlurtMob Comments";
    title.fontSize = 25;
    title.marginBottom = 30;

    // Create axes
    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "author";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.labels.template.rotation = -90;
    categoryAxis.renderer.labels.template.horizontalCenter = "right";
    categoryAxis.renderer.labels.template.verticalCenter = "middle";
    categoryAxis.renderer.minGridDistance = 30;

    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.title.text = "Comment Count";
    valueAxis.tooltip.disabled = true;
    valueAxis.renderer.minWidth = 10;

    // Create series
    var series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = "value";
    series.dataFields.categoryX = "author";
    series.name = "Comments";
    series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
    series.columns.template.fillOpacity = .8;

    var columnTemplate = series.columns.template;
    columnTemplate.strokeWidth = 2;
    columnTemplate.strokeOpacity = 1;

    chart.cursor = new am4charts.XYCursor();

    this.chart = chart;
  }

  getData = async (query) => {
    let apiUrl = ""
    if (this.state.useV2) {
      apiUrl = `/api/blurtmob/tweets_v2?${qs.stringify(query)}`;
    } else {
      apiUrl = `/api/blurtmob/tweets?${qs.stringify(query)}`;
    }
    const response = await api.getUrl(apiUrl);
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  handleFilter = () => {
    const { parent_permlink, min_count } = this.state;
    const query = { parent_permlink, min_count };
    this.setState({ loading: true });
    this.getData(query)
      .then(res => {
        this.setState({ items: res.data, loading: false })
        if (this.state.isDisplayChart) {
          this.createChart();
        }
      })
  }

  createTableColumns = async () => {
    this.columns = [
      {
        title: 'Author',
        dataIndex: '_id',
        key: '_id',
        render: _id => <span>{_id}</span>,
      },
      {
        title: 'Count',
        dataIndex: 'count',
        key: 'count',
        defaultSortOrder: 'descend',
        sorter: (a, b) => a.count - b.count,
        render: count => <span>{count}</span>,
      },
      {
        title: 'Link',
        dataIndex: 'permlinks',
        key: 'permlinks',
        render: (text, record) => (
          <span>
            {
              record.permlinks.map(item => {
                let color = "";
                let permlink = "";
                if (typeof item === "string") {
                  color = "blue"
                  permlink = item
                } else {
                  color = (item[1] || item[1] === "true") ? "orange" : "blue"
                  permlink = item[0]
                }
                return (
                  <Tag color={color}>
                    <a
                      href={`https://blurt.world/@${record._id}/${permlink}`}
                      target="_blank"
                      rel="noopener noreferrer"
                    >Link</a>
                  </Tag>
                )
              })
            }
          </span>
        ),
      },
    ];
  }

  render() {
    const { parent_permlink, min_count, loading, isDisplayChart, items } = this.state
    const disableFilter = !parent_permlink;
    if (!isDisplayChart) {
      this.createTableColumns();
    }

    let modResult = [];
    let i = 0;
    items.forEach(item => {
      i++;
      modResult.push({ ...item, key: i, rowKey: { _id: item._id, rowNum: i } });
    });

    return (
      <PageHeader>
        <div className="wrap">
          <div className="extraContent">
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={24}>
                <Form {...formItemLayout}>
                  <Form.Item label="Permlink">
                    <Input
                      defaultValue={parent_permlink}
                      onChange={e => this.setState({ parent_permlink: e.target.value })}
                    />
                  </Form.Item>
                  <Form.Item label="Min. Comment Count">
                    <InputNumber
                      defaultValue={min_count}
                      onChange={value => this.setState({ min_count: value })}
                      min={1}
                    />
                  </Form.Item>
                  <Form.Item label="Chart/Table">
                    <Switch
                      style={{ marginLeft: 5 }}
                      checkedChildren="Chart"
                      unCheckedChildren="Table"
                      defaultChecked={isDisplayChart}
                      onChange={(checked) => {
                        this.setState({ isDisplayChart: checked, items: [] });
                      }}
                    />
                    {!isDisplayChart &&
                      <Checkbox
                        style={{ marginLeft: 10 }}
                        onChange={e => this.setState({ useV2: e.target.checked })}
                      >Use V2</Checkbox>
                    }
                  </Form.Item>
                  <Form.Item {...tailLayout}>
                    <Button
                      onClick={this.handleFilter}
                      disabled={disableFilter}
                    > Filter</Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
            {loading &&
              <Row type="flex" justify="center">
                <Col xs={24} sm={24} md={24} lg={12} style={{ textAlign: "center" }}>
                  <Spin size="large" />
                </Col>
              </Row>
            }
            {!loading && isDisplayChart &&
              <Row gutter={16} style={{margin: "10px"}}>
                <Col xs={24} sm={24} md={24} lg={24}>
                  <div id="chartdiv" style={{ width: "100%", height: "500px" }}></div>
                </Col>
              </Row>
            }
            {!loading && !isDisplayChart &&
              <Row gutter={16} style={{margin: "10px"}}>
                <Col xs={24} sm={24} md={24} lg={24}>
                  {!_.isEmpty(modResult) &&
                    <Table
                      columns={this.columns}
                      dataSource={modResult}
                      pagination={false}
                    />
                  }
                </Col>
              </Row>
            }
          </div>
        </div>
      </PageHeader>

    );
  }
}

export default AccountsCreatedChart;