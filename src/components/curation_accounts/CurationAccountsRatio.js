import React, { Component } from 'react';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

import {
  PageHeader, Row, Col, Spin, Form, Button,
  InputNumber, Select,
} from 'antd';

import * as qs from 'query-string';
import api from '../../apis/blurtopianAPI';
import blurtAPI from '../../apis/blurtAPI';

import 'antd/dist/antd.css';
import './Home.css';

am4core.useTheme(am4themes_animated);

const { Option } = Select;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 12 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 12 },
  },
};

const tailLayout = {
  wrapperCol: { offset: 12, span: 12 },
};

class CurationAccountsRatio extends Component {

  constructor(props) {
    super(props)
    this.state = {
      global_properties: {},
      curation_accounts: [],
      active_accounts: [],
      active_accounts_count: 0,
      global_total_vests: 0,
      ca_total_vests: 0,
      ac_total_vests: 0,
      days: 30,
      totalMode: '',
      useV2: false,
      items: [],
      loading: false,
    }
  }

  componentWillUnmount() {
    if (this.chart) {
      this.chart.dispose();
    }
  }

  createChart = (nonca_total_vests, ca_total_vests) => {
    console.log('create chart ca_total_vests', ca_total_vests)
    console.log('create chart total_vests', nonca_total_vests)
    // Create chart instance
    let chart = am4core.create("chartdiv", am4charts.PieChart);

    // Add data
    chart.data = [
      {
        "label": "Non-Curation Accounts",
        "value": nonca_total_vests / 1e6
      },
      {
        "label": "Curation Accounts",
        "value": ca_total_vests / 1e6
      }
    ];

    // Add and configure Series
    let pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = "value";
    pieSeries.dataFields.category = "label";
    pieSeries.slices.template.stroke = am4core.color("#fff");
    pieSeries.slices.template.strokeWidth = 2;
    pieSeries.slices.template.strokeOpacity = 1;

    // This creates initial animation
    pieSeries.hiddenState.properties.opacity = 1;
    pieSeries.hiddenState.properties.endAngle = -90;
    pieSeries.hiddenState.properties.startAngle = -90;

    pieSeries.ticks.template.disabled = true;
    pieSeries.alignLabels = false;
    pieSeries.labels.template.text = "{value.percent.formatNumber('#.0')}%";
    pieSeries.labels.template.radius = am4core.percent(-40);
    pieSeries.labels.template.fill = am4core.color("white");
    chart.legend = new am4charts.Legend();

    this.chart = chart;
  }

  getDynamicGlobalProperties = async () => {
    const response = await blurtAPI.getUrl2(`/api/getDynamicGlobalProperties`);
    let body = await response.text();
    body = JSON.parse(body)
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  getCurationAccountsVestedShares = async () => {
    let query = {
      account: "initblurt",
      limit: 1000,
    }
    const response = await blurtAPI.getUrl2(`/api/getVestingDelegations?${qs.stringify(query)}`);
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  getAccounts = async (accounts) => {
    let query = { names: accounts }
    const response = await blurtAPI.getUrl2(`/api/getAccounts?${qs.stringify(query, {arrayFormat: 'bracket'})}`);
    let body = await response.text();
    body = JSON.parse(body)
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  getActiveAccounts = async () => {
    let { days } = this.state;
    let query = { days }
    const response = await api.getUrl2(`/api/votes/active?${qs.stringify(query)}`);
    let body = await response.text();
    body = JSON.parse(body)
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  handleFilter = () => {
    let { totalMode } = this.state;
    this.setState({ loading: true });
    this.getCurationAccountsVestedShares()
      .then(res => {
        let ca_total_vests = 0;
        let ca_accounts= [];
        res.forEach(item => {
          let vests = parseInt(item.vesting_shares.replace(" VESTS", ""))
          vests = vests * 1e6;
          ca_total_vests = ca_total_vests + vests;
          ca_accounts.push(item.delegatee)
        })

        if (totalMode === "global") {
          this.getDynamicGlobalProperties()
            .then(res => {
              let global_total_vests = parseInt(res.total_vesting_shares.replace(" VESTS", ""));
              global_total_vests = global_total_vests * 1e6;
              this.setState({ loading: false });
              this.createChart(global_total_vests - ca_total_vests, ca_total_vests);
            })
        } else {
          this.getActiveAccounts().then(res => {
            let activeAccounts = []
            let active_accounts_count = res.data.length;
            this.setState({ active_accounts_count })

            res.data.forEach(account => {
              this.setState({ loading: true })
              this.getAccounts([account]).then(result => {
                activeAccounts.push(result[0])
                if (activeAccounts.length === active_accounts_count) {
                  console.log('ca_accounts', ca_accounts)
                  let ac_total_vests = 0;
                  let ac_ca_total_vests = 0;
                  activeAccounts.forEach(item => {
                    let vests = parseInt(item.vesting_shares.replace(" VESTS", ""))
                    vests = vests * 1e6
                    let received_vesting_shares = parseInt(item.received_vesting_shares.replace(" VESTS", ""))
                    received_vesting_shares = received_vesting_shares * 1e6;
                    let delegated_vesting_shares = parseInt(item.delegated_vesting_shares.replace(" VESTS", ""))
                    delegated_vesting_shares = delegated_vesting_shares * 1e6;
                    let total_vests = vests + received_vesting_shares - delegated_vesting_shares;
                    if (ca_accounts.indexOf(item.name) >= 0 ) {
                      console.log('item', item)
                      ac_ca_total_vests = ac_ca_total_vests + total_vests;
                    } else {
                      ac_total_vests = ac_total_vests + total_vests;
                    }
                  })
                  this.setState({ loading: false });
                  this.createChart(ac_total_vests, ac_ca_total_vests);
                }
              })
            })
          });
        }
      })
  }

  render() {
    const {
      loading, days, totalMode,
    } = this.state;
    let disableFilter = totalMode === ""

    return (
      <PageHeader>
        <div className="wrap">
          <div className="extraContent">
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={24}>
                <Form {...formItemLayout}>
                  <Form.Item label="Total Vests">
                    <Select
                      defaultValue={totalMode}
                      style={{ width: 150 }}
                      onChange={value => this.setState({ totalMode: value })}
                    >
                      <Option value="global">Global Total Vested</Option>
                      <Option value="active">Active Accounts</Option>
                    </Select>
                  </Form.Item>
                  {totalMode === "active" &&
                    <Form.Item label="Days Active">
                      <InputNumber
                        defaultValue={days}
                        onChange={value => this.setState({ days: value })}
                        min={1}
                      />
                    </Form.Item>
                  }
                  <Form.Item {...tailLayout}>
                    <Button
                      onClick={this.handleFilter}
                      disabled={disableFilter}
                    > Filter</Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
            {loading &&
              <Row type="flex" justify="center">
                <Col xs={24} sm={24} md={24} lg={12} style={{ textAlign: "center" }}>
                  <Spin size="large" />
                </Col>
              </Row>
            }
            {
              <Row gutter={16} style={{margin: "10px", display: loading ? 'none' : ''}}>
                <Col xs={24} sm={24} md={24} lg={24}>
                  <div id="chartdiv" style={{ width: "100%", height: "500px" }}></div>
                </Col>
              </Row>
            }
          </div>
        </div>
      </PageHeader>

    );
  }
}

export default CurationAccountsRatio;