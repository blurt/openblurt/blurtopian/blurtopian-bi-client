import React, { Component } from 'react';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import * as am4plugins_sunburst from "@amcharts/amcharts4/plugins/sunburst";

import {
  PageHeader, Row, Col, Spin, Form, Button,
  InputNumber, Select, Checkbox,
} from 'antd';

import * as qs from 'query-string';
import api from '../../apis/blurtopianAPI';
//import blurtAPI from '../../apis/blurtAPI';
import blurt from'@blurtfoundation/blurtjs';

import 'antd/dist/antd.css';
import './Home.css';

blurt.api.setOptions({ url: 'https://rpc.blurt.world', useAppbaseApi: true });
am4core.useTheme(am4themes_animated);

const { Option } = Select;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

const tailLayout = {
  wrapperCol: { offset: 10, span: 12 },
};

class CurationAccountsRatioV2 extends Component {

  constructor(props) {
    super(props)
    this.state = {
      global_properties: {},
      curation_accounts: [],
      active_accounts: [],
      active_accounts_count: 0,
      global_total_vests: 0,
      ca_total_vests: 0,
      ac_total_vests: 0,
      days: 30,
      totalMode: '',
      useV2: false,
      items: [],
      loading: false,
      displayChart: false,
      displayCuration: true,
      displayNonCuration: true,
      displayInactive: false,
    }
  }

  componentWillUnmount() {
    if (this.chart) {
      this.chart.dispose();
    }
  }

  createChart = (nonca_total_vests, ca_total_vests) => {
    // Create chart instance
    let chart = am4core.create("chartdiv", am4charts.PieChart);

    // Add data
    chart.data = [
      {
        "label": "Curation Accounts",
        "value": ca_total_vests / 1e6
      },
      {
        "label": "Non-Curation Accounts",
        "value": nonca_total_vests / 1e6
      },
    ];

    // Add and configure Series
    let pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = "value";
    pieSeries.dataFields.category = "label";
    pieSeries.slices.template.stroke = am4core.color("#fff");
    pieSeries.slices.template.strokeWidth = 2;
    pieSeries.slices.template.strokeOpacity = 1;

    // This creates initial animation
    pieSeries.hiddenState.properties.opacity = 1;
    pieSeries.hiddenState.properties.endAngle = -90;
    pieSeries.hiddenState.properties.startAngle = -90;

    pieSeries.ticks.template.disabled = true;
    pieSeries.alignLabels = false;
    pieSeries.labels.template.text = "{value.percent.formatNumber('#.0')}%";
    pieSeries.labels.template.radius = am4core.percent(-40);
    pieSeries.labels.template.fill = am4core.color("white");
    chart.legend = new am4charts.Legend();

    this.chart = chart;
    this.setState({ displayChart: true })
  }

  createSunburstChart = (activeAccounts, ca_accounts, global_total_vests) => {
    let children_ca_accounts = [], ac_total_vests = 0;
    let children_non_ca_accounts = [], ac_ca_total_vests = 0;
    activeAccounts.forEach(item => {
      let vests = parseInt(item.vesting_shares.replace(" VESTS", ""))
      vests = vests * 1e6
      let received_vesting_shares = parseInt(item.received_vesting_shares.replace(" VESTS", ""))
      received_vesting_shares = received_vesting_shares * 1e6;
      let delegated_vesting_shares = parseInt(item.delegated_vesting_shares.replace(" VESTS", ""))
      delegated_vesting_shares = delegated_vesting_shares * 1e6;
      let total_vests = vests + received_vesting_shares - delegated_vesting_shares;
      if (ca_accounts.indexOf(item.name) >= 0 ) {
        ac_ca_total_vests = ac_ca_total_vests + total_vests;
        children_ca_accounts.push({ name: item.name, value: total_vests / 1e6 });
      } else {
        ac_total_vests = ac_total_vests + total_vests;
        children_non_ca_accounts.push({ name: item.name, value: total_vests / 1e6 });
      }
    })
    // sort children largest to smallest
    children_ca_accounts = children_ca_accounts.sort((a,b) => b.value - a.value)
    children_non_ca_accounts = children_non_ca_accounts.sort((a,b) => a.value - b.value)

    // Create chart instance
    let chart = am4core.create("chartdiv", am4plugins_sunburst.Sunburst);
    chart.padding(0,0,0,0);
    chart.radius = am4core.percent(98);

    chart.data = [];
    if (this.state.displayCuration) {
      chart.data.push({
        name: "Curation",
        children: children_ca_accounts,
      });
    }
    if (this.state.displayNonCuration) {
      chart.data.push({
        name: "Non-Curation",
        children: children_non_ca_accounts,
      });
    }
    if (this.state.displayInactive) {
      chart.data.push({
        name: "Inactive",
        value: (global_total_vests - ac_ca_total_vests - ac_total_vests) / 1e6
      });
    }

    chart.colors.step = 2;
    chart.fontSize = 11;
    chart.innerRadius = am4core.percent(10);

    // define data fields
    chart.dataFields.value = "value";
    chart.dataFields.name = "name";
    chart.dataFields.children = "children";


    let level0SeriesTemplate = new am4plugins_sunburst.SunburstSeries();
    level0SeriesTemplate.hiddenInLegend = false;
    chart.seriesTemplates.setKey("0", level0SeriesTemplate)

    // this makes labels to be hidden if they don't fit
    level0SeriesTemplate.labels.template.truncate = true;
    level0SeriesTemplate.labels.template.hideOversized = true;

    level0SeriesTemplate.labels.template.adapter.add("rotation", function(rotation, target) {
      target.maxWidth = target.dataItem.slice.radius - target.dataItem.slice.innerRadius - 10;
      target.maxHeight = Math.abs(target.dataItem.slice.arc * (target.dataItem.slice.innerRadius + target.dataItem.slice.radius) / 2 * am4core.math.RADIANS);

      return rotation;
    })


    let level1SeriesTemplate = level0SeriesTemplate.clone();
    chart.seriesTemplates.setKey("1", level1SeriesTemplate)
    level1SeriesTemplate.fillOpacity = 0.75;
    level1SeriesTemplate.hiddenInLegend = true;

    let level2SeriesTemplate = level0SeriesTemplate.clone();
    chart.seriesTemplates.setKey("2", level2SeriesTemplate)
    level2SeriesTemplate.fillOpacity = 0.5;
    level2SeriesTemplate.hiddenInLegend = true;

    chart.legend = new am4charts.Legend();

    this.chart = chart;
    this.setState({ displayChart: true })
  }

  getDynamicGlobalProperties = async () => {
    const response = await blurt.api.getDynamicGlobalPropertiesAsync();
    return response;
  };

  getCurationAccountsVestedShares = async () => {
    const response = await blurt.api.getVestingDelegationsAsync("initblurt", "", 1000);
    return response;
  };

  getAccounts = async (accounts) => {
    const response = await blurt.api.getAccountsAsync(accounts);
    return response;
  };

  getActiveAccounts = async () => {
    let { days } = this.state;
    let query = { days }
    const response = await api.getUrl2(`/api/votes/active?${qs.stringify(query)}`);
    let body = await response.text();
    body = JSON.parse(body)
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  handleFilter = () => {
    let { totalMode } = this.state;
    this.setState({ loading: true, displayChart: false });
    this.getCurationAccountsVestedShares()
      .then(res => {
        console.log('res', res)
        let ca_total_vests = 0;
        let ca_accounts= [];
        res.forEach(item => {
          let vests = parseInt(item.vesting_shares.replace(" VESTS", ""))
          vests = vests * 1e6;
          ca_total_vests = ca_total_vests + vests;
          ca_accounts.push(item.delegatee)
        })

        if (totalMode === "global") {
          this.getDynamicGlobalProperties()
            .then(res => {
              let global_total_vests = parseInt(res.total_vesting_shares.replace(" VESTS", ""));
              global_total_vests = global_total_vests * 1e6;
              this.setState({ loading: false });
              this.createChart(global_total_vests - ca_total_vests, ca_total_vests);
            })
        } else {
          this.getDynamicGlobalProperties()
            .then(res => {
              let global_total_vests = parseInt(res.total_vesting_shares.replace(" VESTS", ""));
              global_total_vests = global_total_vests * 1e6;
              this.getActiveAccounts().then(res => {
                let activeAccounts = []
                let active_accounts_count = res.data.length;
                this.setState({ active_accounts_count })

                res.data.forEach(account => {
                  this.setState({ loading: true })
                  this.getAccounts([account]).then(result => {
                    activeAccounts.push(result[0])
                    if (activeAccounts.length === active_accounts_count) {
                      this.setState({ loading: false });
                      this.createSunburstChart(activeAccounts, ca_accounts, global_total_vests);
                    }
                  })
                })
              });
            })

        }
      })
  }

  render() {
    const {
      loading, days, totalMode, displayChart,
      displayCuration, displayNonCuration, displayInactive,
    } = this.state;
    let disableFilter = totalMode === ""
      || (totalMode === "active" && !displayCuration && !displayNonCuration && !displayInactive);

    return (
      <PageHeader>
        <div className="wrap">
          <div className="extraContent">
            <Row type="flex" justify="center">
              <Col xs={24} sm={24} md={24} lg={24}>
                <Form {...formItemLayout}>
                  <Form.Item label="Total Vests">
                    <Select
                      defaultValue={totalMode}
                      style={{ width: 180 }}
                      onChange={value => this.setState({ totalMode: value, displayChart: false })}
                    >
                      <Option value="global">Global Total Vested</Option>
                      <Option value="active">Active Accounts</Option>
                    </Select>
                  </Form.Item>
                  {totalMode === "active" &&
                    <Form.Item label="Days Active">
                      <InputNumber
                        defaultValue={days}
                        onChange={value => this.setState({ days: value })}
                        min={1}
                      />
                    </Form.Item>
                  }
                  {totalMode === "active" &&
                    <Form.Item label="Display">
                      <Checkbox
                        style={{ marginLeft: 10 }}
                        defaultChecked={displayCuration}
                        onChange={e => this.setState({ displayCuration: e.target.checked })}
                      >Curation</Checkbox>
                      <Checkbox
                        style={{ marginLeft: 10 }}
                        defaultChecked={displayNonCuration}
                        onChange={e => this.setState({ displayNonCuration: e.target.checked })}
                      >Non-Curation</Checkbox>
                      <Checkbox
                        style={{ marginLeft: 10 }}
                        defaultChecked={displayInactive}
                        onChange={e => this.setState({ displayInactive: e.target.checked })}
                      >Inactive</Checkbox>
                    </Form.Item>
                  }
                  <Form.Item {...tailLayout}>
                    <Button
                      onClick={this.handleFilter}
                      disabled={disableFilter}
                    > Filter</Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
            {loading &&
              <Row type="flex" justify="center">
                <Col xs={24} sm={24} md={24} lg={12} style={{ textAlign: "center" }}>
                  <Spin size="large" />
                </Col>
              </Row>
            }
            <Row gutter={16} style={{margin: "10px", display: displayChart ? '' : 'none'}}>
              <Col xs={24} sm={24} md={24} lg={24}>
                <div id="chartdiv" style={{ width: "100%", height: "500px" }}></div>
              </Col>
            </Row>
          </div>
        </div>
      </PageHeader>

    );
  }
}

export default CurationAccountsRatioV2;