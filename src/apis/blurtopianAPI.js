import Client from '../helpers/client'
require('dotenv').config()

const apiUrl = process.env.REACT_APP_API || 'https://biapi.blurtopian.com';
const api = new Client(apiUrl);

export default api;