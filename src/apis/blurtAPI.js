import Client from '../helpers/client'
require('dotenv').config()

const apiUrl = process.env.REACT_APP_BLURT_REST_API || 'https://api.blurt.buzz';
const api = new Client(apiUrl);

export default api;