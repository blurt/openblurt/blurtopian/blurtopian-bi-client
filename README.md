[![pipeline status](https://gitlab.com/blurt/openblurt/blurtopian/blurtopian-bi-client/badges/master/pipeline.svg)](https://gitlab.com/blurt/openblurt/blurtopian/blurtopian-bi-client/-/commits/master)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

<center>
![image.png](https://images.blurt.buzz/DQmNstwJrD6LE9Dk1pbwXQyKJSVxPmwfaJ6TgnPL68CTiTe/image.png)
</center>

Last week, I posted a [basic analysis](https://blurt.world/blurtopian/@eastmael/blurtopian-analysis-where-is-blurt-now-a-blurt-blockchain-analysis) showcasing the growth in Blurt votes and comments. In this post, I'm sharing a new chart - the blurt vote map.

![image.png](https://images.blurt.buzz/DQmWzfpTctfGqfFW65wpQ7w9bjTSxmzXNa57PLXQxjf1Nrp/image.png)

# 1. Background

In coordination with @megadrive, I developed the tool to provide founders and concerned community members with a tool to help manage the chain.


![image.png](https://images.blurt.buzz/DQmS4BXkZGkFWWEgDDLeo6mTm8gPUmBFWpWTAvQM7oNYrss/image.png)


# 2. Features

This aims to provide a walkthrough of the site's pages.

## 2.1. [Blurt Votes Summary](https://bi.blurt-now.com/charts/votes)

| <center>Home Icon</center> | <center>Page </center> |
| -- | -- |
| ![image.png](https://images.blurt.buzz/DQmaNCFi4xFaqCZQnSwkhH8iyq88x9KSMNyUb1BaZ4UnZ8g/image.png) | ![image.png](https://images.blurt.buzz/DQmdZQURpwaDScBFi3i7Dn5gawVNfdCsmXVevYYscSY8xqU/image.png) |

This page aims to provide an overview of the growth in vote activity. The highest was on **September 22** with **6,838 votes**.

## 2.2. [Votes By Account](https://bi.blurt-now.com/charts/votes_by_count)

| <center>Home Icon</center> | <center>Page </center> |
| -- | -- |
| ![image.png](https://images.blurt.buzz/DQmRhajRWjvh4w3FH1VZr2CgMsCpKVRssvonfYX8P6LQ5wV/image.png) | ![image.png](https://images.blurt.buzz/DQmSrbZ846jhEjBxh8EQTZ2GJy6FDQiBLYu2XGSRoGSRJpa/image.png) |

Want to see who voted most in the chain? Then this chart's for you. From **October 15 to October 22**, the account which had the most votes was @jlufer which casted **423 votes**.

## 2.3. [Blurt Posts and Comments Summary](https://bi.blurt-now.com/charts/comments)

| <center>Home Icon</center> | <center>Page </center> |
| -- | -- |
| ![image.png](https://images.blurt.buzz/DQmWh4RbmFsCTp997LBDLer9jqiVVPRZ8zf9wsYCKAJKrTR/image.png) | ![image.png](https://images.blurt.buzz/DQmaVn7jv6nTMnVW9MusnQdri4WZR88RhjiRBtTgePpfgTP/image.png) |

This was one of the charts I shared in the [previous analysis](https://blurt.world/blurtopian/@eastmael/blurtopian-analysis-where-is-blurt-now-a-blurt-blockchain-analysis). It summarizes the number of posts and comments on the chain. The most posts and comments was on **October 11** with **932 comments** and **788 posts**.

# 2.4. [Blurt Account Votes Network](https://bi.blurt-now.com/votes/votes_map_vis_v1)

| <center>Home Icon</center> | <center>Page </center> |
| -- | -- |
| ![image.png](https://images.blurt.buzz/DQmRgiarCfS7N2Ax4hwxe9XfpJ1Wyg13Zrv74ECj7SjEuqx/image.png) | ![image.png](https://images.blurt.buzz/DQmZ3dHtL2pDgYoQQsCrEK1P7KDchdBiSBRmeFvAMpWP7tF/image.png) |

Given a date range, you'll get a visual of the accounts you voted for the votes these accounts casted. Fun right? :D

# 2.5. [Votes Table](https://bi.blurt-now.com/votes/votes_table)

| <center>Home Icon</center> | <center>Page </center> |
| -- | -- |
| ![image.png](https://images.blurt.buzz/DQmVdJzyVriixmvWLuQ1KhYXXV129NooU6kSnNawpGiqtYy/image.png) | ![image.png](https://images.blurt.buzz/DQmWieCZtvRjHhaAUWmh9fb5CmquUWddLbGXRsc2hzkwDTz/image.png) |

Want to see who voted for you most? Then this chart's for you. In the table above, I received **41 votes** from **October 15 to 22**.

# 2.6. [Blurt Accounts Created - Chart](https://bi.blurt-now.com/accounts/created)

As introduced, the new page is the accounts created page. It has two modes - **chart** and **table**.

| <center>Home Icon</center> | <center>Page </center> |
| -- | -- |
| ![image.png](https://images.blurt.buzz/DQmQHfgkJEXec8qTZCHQ91s7TEhwd9xZrrZ6ZLFZXnim1aN/image.png) | ![image.png](https://images.blurt.buzz/DQma81nNeRQURD1rjRYaaruutV6L7bPdYjrwMecGuJdJ8LD/image.png) |

Let's look at these two modes.

## 2.6.1. Chart Mode

| <center>Home Icon</center> | <center>Page </center> |
| -- | -- |
| ![image.png](https://images.blurt.buzz/DQmX3u6tt7gUUbd4c1gPALe43oFDsahqXfsW1gdUdwuQ7vr/image.png) | ![image.png](https://images.blurt.buzz/DQmWeDW9cCDqXW4WsArPA4NPwTLyrAYGzqJeDYjqzZNYemP/image.png) |

This page aims to provide an overview of the number of accounts created by date. The highest was on **October 9** with **32 accounts**. To see the breakdown of this is the function of the next page.

## 2.6.2. Table Mode

If you want to see the breakdown of the accounts created in the chart above, just switch to **table** mode and click filter.

| <center>Home Icon</center> | <center>Page </center> |
| -- | -- |
| ![image.png](https://images.blurt.buzz/DQmX3u6tt7gUUbd4c1gPALe43oFDsahqXfsW1gdUdwuQ7vr/image.png) | ![image.png](https://images.blurt.buzz/DQmPKUNNj6ajnTwajw1vDsYRPzJ3RbwoedpDMXm9iyk8XUk/image.png) |

As we can see from the table above, on **October 9**, @blurt.buzz created **31 accounts** while @wasif33 created **1 account**.

# 3. Repository

The project's still on its early stage and there are still a couple of bugs I found. Only front-end or client is open-sourced for now.

https://gitlab.com/blurt/openblurt/blurtopian/blurtopian-bi-client

# 4. Feedback and Comments

Please feel free to create a ticket in the project's repository or leave a comment in this post.

# 5. Credits

Thanks to @megadrive and @jacobgadikian for this project.

***

Find my contributions valuable? Please consider voting for my witness:

https://blurtwallet.com/~witnesses?highlight=eastmael

![Banner3_Orange.png](https://img.blurt.world/blurt/eastmael/38fb3c761d2f87a2b5170732d3d175d1c78e6551.png)
